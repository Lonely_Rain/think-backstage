<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/11/29
 * Time: 11:44
 */

namespace app\service\interfaces;


interface FileHandlerInterface
{
    /**
     * 复制文件
     *
     * @param string $path
     * @return array
     */
    public function cp(string $path):array;

    /**
     * 输入文件流
     *
     * @param string $suffix       文件后缀
     * @param int $type            1 图片 2 视频
     * @param string $content      文件原始内容
     * @return array
     */
    public function put(string $suffix, int $type, string $content):array;

    /**
     * 输出文件流
     *
     * @param string $path 全路径
     * @param bool $isDownload 是否下载
     * @param string $name 下载名称
     * @param bool $simple 是否极简处理（读取有效）
     * @return mixed
     */
    public function out(string $path, bool $isDownload, string $name = '', bool $simple = true);

    /**
     * 验证资源是否存在
     *
     * @param string $path
     * @return array
     */
    public function exists(string $path):array;
}
