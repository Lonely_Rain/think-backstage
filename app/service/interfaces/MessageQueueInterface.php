<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/11/27
 * Time: 10:06
 */

namespace app\service\interfaces;


interface MessageQueueInterface
{
    /**
     * 订阅频道
     *
     * @param array $channel
     * @param string $client
     */
    public function subscribe(array $channel, string $client): void;

    /**
     * 广播频道
     *
     * @param string $channel
     * @param array $data
     * @return void
     */
    public function publish(string $channel, array $data): void;

    /**
     * 获取频道中的数据
     *
     * @param string $channel
     * @param string $client
     * @param int $num
     * @return array
     */
    public function data(string $channel, string $client, int $num): array;

    /**
     * 关闭频道
     *
     * @param array $channel
     * @return void
     */
    public function close(array $channel): void;
}
