<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/11/27
 * Time: 10:06
 */

namespace app\service\interfaces;


interface EncryptionInterface
{
    /**
     * 加密
     *
     * @param string $data
     * @param string $key
     * @param int $time 时效性 0 永久
     * @return string
     */
    public function encryption(string $data, string $key = '', int $time = 0): string;

    /**
     * 解密
     *
     * @param string $data
     * @param string $key
     * @return string
     */
    public function decrypt(string $data, string $key = ''): string;
}
