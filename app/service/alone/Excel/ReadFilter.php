<?php


namespace app\service\alone\Excel;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class ReadFilter implements IReadFilter
{
    /** @var array $rows 读取行数 */
    private $rows = ['start' => '开始行', 'end' => '结束行'];

    /** @var array $column 读取列数 */
    private $column;

    public function __construct(array $rows, array $column = [])
    {
        $this->rows = $rows;

        $this->column = $column;
    }

    public function readCell($column, $row, $worksheetName = '')
    {
        return $row >= $this->rows['start'] && $row <= $this->rows['end'] && (!$this->column || in_array($column, $this->column));
    }
}