<?php


namespace app\service\alone\City;


use app\service\alone\BaseService;
use app\service\alone\City\Models\CityModel;

/**
 * Class CityService
 *
 * @property-read CityModel $model
 *
 * @package app\service\alone\City
 */
class CityService extends BaseService
{
    protected $rely = [
        'model' => CityModel::class
    ];

    /** @var bool 是否分页 */
    private $isPage = false;

    /** @var int 页总数 */
    private $page = 10;

    protected function init()
    {
        if (isField($this->data, 'isPage')) $this->isPage = $this->data['isPage'];

        if (isField($this->data, 'page')) $this->page = $this->data['page'];
    }

    /**
     * 获取省列表
     *
     * @return mixed
     */
    public function province()
    {
        return $this->model->getList($this->isPage, $this->page, ['province_id' => 0]);
    }

    /**
     * 获取市列表
     *
     * @param int $provinceId
     * @return mixed
     */
    public function city(int $provinceId)
    {
        return $this->model->getList($this->isPage, $this->page, ['province_id' => $provinceId, 'city_id' => 0]);
    }

    /**
     * 获取区列表
     *
     * @param int $cityId
     * @return mixed
     */
    public function area(int $cityId)
    {
        return $this->model->getList($this->isPage, $this->page, ['city_id' => $cityId]);
    }
}