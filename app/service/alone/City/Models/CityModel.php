<?php


namespace app\service\alone\City\Models;


use app\model\Area;
use app\service\alone\BaseService;
use app\service\traits\Query;

class CityModel extends BaseService
{
    use Query;

    /**
     * 获取列表
     *
     * @param bool $isPage
     * @param int $page
     * @param array $where
     * @return mixed
     */
    public function getList(bool $isPage = false, int $page = 10, array $where = [])
    {
        $this->isPage = $isPage;
        $this->page = $page;

        $query = Area::field(['id', 'name', 'code'])->order('sort desc,id asc');

        // 条件过滤
        if ($where) {
            if (isField($where, 'province_id')) $query->where('pid->province_id', $where['province_id']);

            if (isField($where, 'city_id')) $query->where('pid->city_id', $where['city_id']);

            if (isField($where, 'id') && $where['id']) $query->where('id', $where['id']);

            if (isField($where, 'code')) $query->where('code', $where['code']);
        }

        return $this->all($query);
    }
}