<?php

namespace app\service\alone\Cache;

use app\service\alone\BaseService;
use think\facade\Cache;

class RedisService extends BaseService
{
    protected $redis = false;

    protected function init()
    {
        $this->redis = Cache::store('redis')->handler();
    }

    /**
     * 获取 redis 实例
     *
     * @return bool
     */
    public function make()
    {
        return $this->redis;
    }

    /**
     * 添加数据到队列
     *
     * 方式：插入的队列的末尾
     *
     * @param array $data
     * @param string $prefix
     * @return void
     */
    public function push(array $data, string $prefix)
    {
        // 添加到队列
        array_reduce($data, function ($result, $v) use ($prefix) {
            $this->redis->rpush($prefix, $v);
        }, '');
    }

    /**
     * 从左边第一个元素开始移除并返回移除的值
     *
     * @param string $prefix
     * @return void
     */
    public function pop(string $prefix)
    {
        return $this->redis->lpop($prefix);
    }

    /**
     * 获取值
     *
     * @param string $prefix 标识
     * @param integer $type 0 单个值 1 列表
     * @return void
     */
    public function get(string $prefix, int $type = 0)
    {
        switch ($type) {
            case 0:
                $result = $this->redis->get($prefix) ? unserialize($this->redis->get($prefix)) : false;
                break;
            case 1:
                $result = $this->redis->lrange($prefix, 0, -1);
                break;
            default:
                $result = false;
        }

        return $result;
    }

    /**
     * 设置值
     *
     * @param string $prefix 标识
     * @param [type] $value     值 （序列化处理，可以为数组等类型）
     * @param integer $second 过期时间(秒)，不可重复key
     * @return mixed
     */
    public function set(string $prefix, $value, int $second = 0)
    {
        // 序列化
        $value = serialize($value);

        // 设置过期时间，不允许设置重复 key
        if ($second) return $this->redis->set($prefix, $value, ['nx', 'ex' => $second]);

        // 设置值
        return $this->redis->set($prefix, $value);
    }

    /**
     * 删除键（包括列表）
     *
     * @param string $prefix
     * @return void
     */
    public function clear(string $prefix)
    {
        return $this->redis->del($prefix);
    }
}
