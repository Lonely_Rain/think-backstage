<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/21
 * Time: 13:42
 */

namespace app\service\alone;


use app\service\traits\Rely;
use app\service\traits\SingleCase;

class BaseService
{
    use SingleCase, Rely;
}
