<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 14:03
 */

namespace app\service\traits;


use app\service\alone\Cache\ModelService;

trait Query
{
    // 是否分页
    public $isPage = false;

    // 是否简洁分页
    public $simple = false;

    public $page = 10;

    /**
     * 缓存处理
     *
     * @param $query
     * @param string $mark  缓存标识，强制开启
     * @return mixed
     */
    protected function cacheHandler($query, $mark){
        // 获取配置值
        $config = ModelService::getInstance() -> boot($class = get_class($query -> getModel()));

        // 是否开启缓存
        if($config && $mark) ModelService::getInstance() -> setCache($config, $query, $class . '_' . $mark);

        return $query;
    }

    /**
     * 清除缓存
     *
     * @param $className
     */
    protected function clearCache($className){
        // 获取配置值
        $config = ModelService::getInstance() -> boot($className);

        // 开启缓存则清除缓存
        if($config) ModelService::getInstance() -> clearCache($config);
    }

    /**
     * 查询多条
     *
     * @param $query
     * @param string $mark
     * @return mixed
     */
    protected function all($query, string $mark = ''){
        if($this -> isPage) return $query -> paginate($this -> page, $this -> simple, ['query'=>request()->param()]);

        return $this -> cacheHandler($query, $mark) -> select();
    }

    /**
     * 查询单条
     *
     * @param $query
     * @param string $mark
     * @return mixed
     */
    protected function one($query, string $mark = ''){
        return $this -> cacheHandler($query, $mark) -> find();
    }

    /**
     * 单个数据
     *
     * @param $query
     * @param string $field
     * @param string $mark
     * @return mixed
     */
    protected function value($query, string $field, string $mark = ''){
        return $this -> cacheHandler($query, $mark) -> value($field);
    }

    /**
     * 获取一列
     *
     * @param $query
     * @param string $field
     * @param string $mark
     * @return mixed
     */
    protected function column($query, string $field, string $mark = ''){
        return $this -> cacheHandler($query, $mark) -> column($field);
    }

    /**
     * 创建数据
     *
     * @param $query
     * @param array $data
     * @return mixed
     */
    protected function add($query, array $data){
        $result = $query::create($data);

        // 清除缓存
        $this -> clearCache($query);

        return $result;
    }

    /**
     * 更新数据
     *
     * @param $query
     * @param array $data
     * @return mixed
     */
    protected function update($query, array $data){
        $result = $query::update($data);

        // 清除缓存
        $this -> clearCache($query);

        return $result;
    }

    /**
     * 更新多条数据
     *
     * @param $query
     * @param array $data
     * @return mixed
     */
    protected function updateAll($query, array $data){
        $result = $query -> saveAll($data);

        // 清除缓存
        $this -> clearCache($query);

        return $result;
    }

    /**
     * 删除数据
     *
     * @param $query
     * @param array $ids
     * @return mixed
     */
    protected function del($query, array $ids){
        $result = $query::destroy($ids);

        $this -> clearCache($query);

        return $result;
    }
}
