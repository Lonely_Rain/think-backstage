<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/21
 * Time: 23:40
 */

namespace app\resource;


use app\BaseController;
use app\service\alone\Encryption\EncryptionService;
use think\facade\Filesystem;

class FileController extends BaseController
{
    private $encryption;

    protected function initialize()
    {
        $this->encryption = EncryptionService::getInstance();
    }

    /**
     * 处理文件
     *
     * @param string $input
     * @return \think\Response|\think\response\Redirect
     */
    public function handler(string $input)
    {
        // 资源验证失败 跳转404
        if (!$path = $this->encryption->decrypt($input)) return redirect((string)url('404'));

        // 资源不存在
        if (!is_file($path = Filesystem::disk('public')->path($path))) return redirect((string)url('404'));

        // 响应输出文件流
        return response()->contentType(mime_content_type($path))->data(file_get_contents($path));
    }
}
