<?php
declare (strict_types = 1);

namespace app\middleware;

use app\admin\service\business\Alone\Config\ConfigService;
use think\facade\View;

class Backstage
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        // 获取页面变量
        $list = ConfigService::getInstance() -> backstage();

        View::assign('adminPage', $list);

        //
        return $next($request);
    }
}
