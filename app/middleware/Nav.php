<?php
declare (strict_types = 1);

namespace app\middleware;

use app\admin\service\business\Alone\Nav\NavService;

class Nav
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        // 导航栏
        if($request -> method() == 'GET' && !$request -> isAjax()) NavService::getInstance() -> boot();

        return $next($request);
    }
}
