<?php
declare (strict_types = 1);

namespace app\middleware;

use app\service\traits\Unified;

class CheckSign
{
    use Unified;

    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        // 除 GET 请求，全部验证签名
        if('GET' != $request -> method()) {

        }

        return $next($request);
    }
}
