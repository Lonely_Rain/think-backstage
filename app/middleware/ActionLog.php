<?php
declare (strict_types = 1);

namespace app\middleware;

use app\admin\service\business\controllers\Access\Action\ActionService;

class ActionLog
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        // 记录当前日志
        if($request -> rule() -> getRule() != 'action-log') ActionService::getInstance() -> operating();

        return $next($request);
    }
}
