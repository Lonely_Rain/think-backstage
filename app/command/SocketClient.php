<?php
declare (strict_types=1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class SocketClient extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('socket_client')
            ->setDescription('the socket_client command');
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        $output->writeln('socket client start');

        list($ip, $port) = ['127.0.0.1', 8999];

        try {
            // 创建 socket
            if (0 > $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) $output->writeln('创建 socket 失败：' . doEncoding(socket_strerror($sock)));

            // 连接 ip:端口
            $output->writeln("连接 socket，ip为：$ip:$port");
            if (0 > $connect = socket_connect($sock, $ip, $port)) $output->writeln('连接 socket 失败：' . doEncoding(socket_strerror($connect)));

            // 发送信息
            $message = '我是客户端，测试发送消息';
            if (!socket_write($sock, $message, mb_strlen($message))) {
                $output->writeln('发送消息失败：' . doEncoding(socket_strerror($connect)));
            } else {
                $output->writeln('发送消息成功，内容为：' . $message);
            }

            // 接收消息
            while ($out = socket_read($sock, 81920000)) {
                $output->writeln('接收服务器信息，内容为：' . $out);
            }

            // 关闭
            socket_close($sock);
        } catch (\Exception $exception) {
            $output->writeln('操作失败：' . doEncoding($exception->getMessage()));
        }
    }
}
