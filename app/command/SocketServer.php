<?php
declare (strict_types=1);

namespace app\command;

use app\service\alone\Socket\SocketService;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Log;

class SocketServer extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('socket_server')
            ->setDescription('the socket_server command');
    }

    protected function execute(Input $input, Output $output)
    {
        SocketService::getInstance()->boot($output);
    }
}
