<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class AdminUsers extends Model
{
    protected $autoWriteTimestamp = true;

    /**
     * 当前拥有的角色
     *
     * @return \think\model\relation\BelongsToMany
     */
    public function roles(){
        return $this -> belongsToMany(AdminRole::class, AdminUserRole::class, 'role_id', 'user_id');
    }
}
