<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class AdminMenu extends Model
{
    /**
     * 当前角色可访问的菜单
     *
     * @return \think\model\relation\BelongsToMany
     */
    public function roles(){
        return $this -> belongsToMany(AdminRole::class, AdminRoleMenu::class, 'role_id', 'menu_id');
    }
}
