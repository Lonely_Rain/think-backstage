<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class AdminRole extends Model
{
    /**
     * 当前角色下的权限
     *
     * @return \think\model\relation\BelongsToMany
     */
    public function rules(){
        return $this -> belongsToMany(AdminRule::class, AdminRoleRule::class, 'rule_id', 'role_id');
    }

    /**
     * 拥有当前角色的用户
     *
     * @return \think\model\relation\BelongsToMany
     */
    public function users(){
        return $this -> belongsToMany(AdminUsers::class, AdminUserRole::class, 'user_id', 'role_id');
    }

    /**
     * 当前角色下的菜单
     *
     * @return \think\model\relation\BelongsToMany
     */
    public function menus(){
        return $this -> belongsToMany(AdminMenu::class, AdminRoleMenu::class, 'menu_id', 'role_id');
    }
}
