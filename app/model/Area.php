<?php
declare (strict_types=1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Area extends Model
{
    //
    protected $jsonAssoc = true;

    protected $json = ['pid'];

    protected $jsonType = [
        'pid->province_id' => 'int',
        'pid->city_id' => 'int'
    ];
}
