<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class ArticleCate extends Model
{
    /**
     * 文章列表
     *
     * @return \think\model\relation\HasMany
     */
    public function articles(){
        return $this -> hasMany(Articles::class, 'classify_id', 'id');
    }
}
