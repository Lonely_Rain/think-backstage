<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class AdminActionLog extends Model
{
    //
    /**
     * 管理员
     *
     * @return \think\model\relation\HasOne
     */
    public function admin(){
        return $this -> hasOne(AdminUsers::class, 'id', 'user_id');
    }
}
