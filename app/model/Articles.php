<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class Articles extends Model
{
    /**
     * 文章分类
     *
     * @return \think\model\relation\HasOne
     */
    public function cate(){
        return $this -> hasOne(ArticleCate::class, 'id', 'classify_id');
    }
}
