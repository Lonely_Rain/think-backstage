<?php
declare (strict_types = 1);

namespace app\model;

use think\model\Pivot;

/**
 * @mixin think\Model
 */
class AdminRoleMenu extends Pivot
{
    //
    protected $autoWriteTimestamp = true;
}
