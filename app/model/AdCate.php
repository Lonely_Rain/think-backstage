<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class AdCate extends Model
{
    /**
     * 获取当前分类下的广告
     *
     * @return \think\model\relation\HasMany
     */
    public function ad(){
        return $this -> hasMany(Ad::class, 'cate_id', 'id');
    }
}
