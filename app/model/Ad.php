<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class Ad extends Model
{
    /**
     * 当前广告的分类
     *
     * @return \think\model\relation\HasOne
     */
    public function cate(){
        return $this -> hasOne(AdCate::class, 'id', 'cate_id');
    }
}
