<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class AdminRule extends Model
{
    /**
     * 拥有当前权限的角色
     *
     * @return \think\model\relation\BelongsToMany
     */
    public function roles(){
        return $this -> belongsToMany(AdminRole::class, AdminRoleRule::class, 'role_id', 'rule_id');
    }
}
