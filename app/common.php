<?php

/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2019/8/30
 * Time: 11:43
 */

if (!function_exists('echoArr')) {
    /**
     * 接口返回格式
     *
     * @param $code
     * @param string $msg
     * @param array $data
     * @return array
     */
    function echoArr($code, $msg = '', $data = [])
    {
        return [
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        ];
    }
}

if (!function_exists('getErrorMessage')) {
    /**
     * 错误 提示
     *
     * @param $code
     * @param $config
     * @param string $type
     * @return mixed
     */
    function getErrorMessage($code, $config, $type = 'error')
    {
        // 是否开启错误提示
        if (!$config['config']['error_prompt']) return $config['common'][400];

        // 错误码是否存在
        if (!isset($config[$type][$code])) return $config['common'][401];

        // 具体错误信息
        return $config[$type][$code];
    }
}

if (!function_exists('arrConversion')) {
    /**
     * 数组处理
     *
     * @param $list
     * @param string $unique
     * @return mixed
     */
    function arrConversion($list, $unique = 'id')
    {
        return array_column($list, null, $unique);
    }
}

if (!function_exists('randomNum')) {
    /**
     * 生成唯一值
     *
     * @param int $uid 唯一值
     * @param int $length 长度
     * @return string
     */
    function randomNum($uid = 1, int $length = 0)
    {
        $num = substr(uniqid(), 7, 13);
        $str = '';
        for ($i = 0; $i < strlen($num); $i++) {
            $str .= ord($num[$i]);
        }
        $str = date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT) . $uid . substr($str, 0, 10);

        return $length !== 0 ? substr($str, -$length) : $str;
    }
}

if (!function_exists('cateLevel')) {
    /**
     * 分类级别处理
     *
     * @param $list
     * @param int $pid
     * @param int $level
     * @return array
     */
    function cateLevel($list, $pid = 0, $level = 1)
    {
        $temp = [];
        foreach ($list as $k => $v) {
            if ($pid == $v['pid']) {
                $v['level'] = $level;

                $v['child'] = cateLevel($list, $v['id'], $level + 1);

                $temp[] = $v;
            }
        }

        return $temp;
    }
}

if (!function_exists('isField')) {
    /**
     * 判断字段是否存在
     *
     * @param array $data
     * @param string $key
     * @return bool
     */
    function isField(array $data, string $key): bool
    {
        if (!isset($data[$key])) return false;

        if (!$data[$key]) {
            if ($data[$key] === '0' || $data[$key] === 0) return true;

            return false;
        }

        return true;
    }
}

if (!function_exists('handlerCateList')) {
    /**
     * 处理分类列表（无限级）
     *
     * @param array $list
     * @param string $pidKey
     * @param bool $quote 是否删除引用 默认为true
     * @param array $quoteIds 被引用的数组列表
     * @return array
     */
    function handlerCateList(array $list, string $pidKey = 'pid', bool $quote = true, array &$quoteIds = []): array
    {
        list($temp, $tree) = [[], []];

        // 初始化 key => $v['id']
        foreach ($list as $v) {
            $v['tempLevel'] = [];

            $tree[$v['id']] = $v;
            $tree[$v['id']]['children'] = [];
        }

        // 引用子节点
        foreach ($tree as $k => $item) {
            // 父级没有引用节点
            if ($item[$pidKey] != 0) {
                if (!isset($tree[$item[$pidKey]])) continue;

                $tree[$item[$pidKey]]['children'][] = &$tree[$k];

                $tree[$k]['tempLevel'][] = &$tree[$item[$pidKey]]['tempLevel'];

                $temp[] = $item['id'];
            } else {
                $tree[$k]['tempLevel'] = [true];
            }
        }

        // 计算级别，清除级别引用
        foreach ($tree as $k => $v) {
            $tree[$k]['level'] = count($v['tempLevel'], 1);

            unset($tree[$k]['tempLevel']);
        }

        // 是否取消引用
        if ($quote) {
            // 取消引用，并清除多余数据
            foreach ($temp as $k => $v) {
                unset($tree[$v]);
            }
        }

        // 被引用的数组
        $quoteIds = $temp;

        return $tree;
    }
}

if (!function_exists('getTopId')) {
    /**
     * 获取子分类的顶级id
     *
     * @param array $list 分类列表
     * @param string $pidKey 父级键名
     * @param int $subId 当前分类id
     * @param array $iteration 统计当前分类上的所有父级
     * @return int
     */
    function getTopId(array $list, string $pidKey, int $subId, array &$iteration): int
    {
        list($newList, $iteration) = [[], []];

        // 处理数据格式
        foreach ($list as $v) {
            $newList[$v['id']] = $v[$pidKey];
        }

        // 获取顶级id
        while (isset($newList[$subId]) && $newList[$subId]) {
            $subId = $newList[$subId];

            array_push($iteration, $subId);
        }

        return $subId;
    }
}

if (!function_exists('interceptStr')) {
    /**
     * 截取字符串
     *
     * @param string $str 需截取的字符串
     * @param int $max 截取的位数
     * @param bool $isPlainText 是否为富文本
     * @return string
     */
    function interceptStr(string $str, int $max = 60, bool $isPlainText = true): string
    {
        $old_str = $isPlainText ? $str : strip_tags($str);

        $str = mb_substr($old_str, 0, $max, 'utf-8');

        if (mb_strlen($old_str, 'utf-8') > $max) $str .= '....';

        return $str;
    }
}

if (!function_exists('doEncoding')) {
    /**
     * 转换字符串编码 utf-8
     *
     * @param string $str
     * @return string
     */
    function doEncoding(string $str): string
    {
        $encode = strtoupper(mb_detect_encoding($str, ["ASCII", 'UTF-8', "GB2312", "GBK", 'BIG5']));

        if ($encode != 'UTF-8') $str = mb_convert_encoding($str, 'UTF-8', $encode);

        return $str;
    }
}

if (!function_exists('parsingHeaders')) {
    /**
     * 解析 header 头信息
     *
     * @param string $header
     * @return array
     */
    function parsingHeaders(string $header): array
    {
        $list = explode("\r\n", $header);
        array_pop($list);
        array_pop($list);

        return array_reduce($list, function ($result, $v) {
            $arr = explode(': ', $v);

            if (isset($arr[1])) {
                $result[$arr[0]] = $arr[1];
            } else {
                $tmp = explode(' / ', $arr[0]);

                $result['Http'] = $tmp[1];
                $result['Method'] = $tmp[0];
            }

            return $result;
        }, []);
    }
}
