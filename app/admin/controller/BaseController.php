<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/15
 * Time: 22:04
 */

namespace app\admin\controller;


use app\admin\service\business\BaseControllerService;
use app\admin\validate\Common;
use app\service\traits\Unified;

/**
 * Class BaseController
 *
 * @property-read BaseControllerService $server
 *
 * @package app\admin\controller
 */
class BaseController extends \app\BaseController
{
    use Unified;

    /**
     * 修改状态
     *
     * @param int $id
     * @return object
     */
    public function status(int $id){
        $params = $this -> request -> param(['status']);

        // 验证请求参数
        $this -> validate($params, Common::class . '.status');

        // 处理逻辑
        $result = $this -> server -> editIndividualValue($id, $params, 1);

        return $this -> returnData($result['code'], $result['msg'], $result['data']);
    }

    /**
     * 修改排序
     *
     * @param int $id
     * @return object
     */
    public function sort(int $id){
        $params = $this -> request -> param(['sort']);

        // 验证请求参数
        $this -> validate($params, Common::class . '.sort');

        // 处理逻辑
        $result = $this -> server -> editIndividualValue($id, $params, 1);

        return $this -> returnData($result['code'], $result['msg'], $result['data']);
    }
}
