<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\admin\service\business\controllers\Access\Action\ActionService;
use think\facade\View;

class AdminActionLogController extends BaseController
{
    /** @var ActionService */
    protected $server;

    protected function initialize()
    {
        $this -> server = ActionService::getInstance();
    }

    /**
     * 显示资源列表
     *
     * @return string
     * @throws \Exception
     */
    public function index()
    {
        if($this -> request -> isAjax()){
            // 请求参数
            $params = $this -> request -> get(['key']);

            return $this -> returnData(200, '请求成功', [
                'list' => $this -> server -> resList($params, $this -> request -> get('limit', 10, 'intval'))
            ]);
        }

        //
        return View::fetch('index', [
            'methods' => $this -> server -> methods(),
            'admins' => $this -> server -> getAdminList()
        ]);
    }
}
