<?php
declare (strict_types=1);

namespace app\admin\controller;

use app\admin\validate\Upload;
use app\service\alone\Encryption\EncryptionService;
use app\service\alone\Upload\UploadService;

class UploadController extends BaseController
{
    /** @var UploadService */
    protected $server;

    protected function initialize()
    {
        $this->server = UploadService::getInstance();
    }

    /**
     * 上传图片
     *
     * @return object
     * @throws \Exception
     */
    public function img()
    {
        $files = $this->request->file();

        $this->validate($files, Upload::class . '.image');

        $result = $this->server->uploadImg($files['img'], EncryptionService::getInstance());

        return $this->returnData(200, '上传成功', $result);
    }

    /**
     * 上传视频
     *
     * @return object
     * @throws \Exception
     */
    public function video()
    {
        $files = $this->request->file();

        $this->validate($files, Upload::class . '.video');

        $result = $this->server->uploadVideo($files['video'], EncryptionService::getInstance());

        return $this->returnData(200, '上传成功', $result);
    }

    /**
     * 上传文件
     *
     * @return object
     * @throws \Exception
     */
    public function file()
    {
        $files = $this->request->file();

        $this->validate($files, Upload::class . '.file');

        $result = $this->server->uploadFile($files['file'], EncryptionService::getInstance());

        return $this->returnData(200, '上传成功', $result);
    }

    /**
     * 分片上传
     *
     * @param string $action
     * @return \think\Response
     */
    public function multipartUpload(string $action)
    {
        $result = $this->server->multipart($action);

        return $this->returnData($result['code'], $result['msg'], $result['data']);
    }
}
