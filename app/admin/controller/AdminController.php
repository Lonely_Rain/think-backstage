<?php
declare (strict_types=1);

namespace app\admin\controller;

use app\admin\service\business\controllers\Access\Achieve\HandlerRoleModel;
use app\admin\service\business\controllers\Access\Admin\AdminService;
use app\admin\validate\AdminUser;
use think\App;
use think\facade\View;
use think\Request;

class AdminController extends BaseController
{
    /** @var AdminService */
    protected $server;

    protected function initialize()
    {
        $this->server = AdminService::getInstance();
    }

    /**
     * 显示资源列表
     *
     * @return string
     * @throws \Exception
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $params = $this->request->get(['key']);

            return $this->returnData(200, '请求成功', [
                'list' => $this->server->resList($params, $this->request->get('limit', 10, 'intval'))
            ]);
        }

        //
        return View::fetch();
    }

    /**
     * 显示创建资源表单页
     *
     * @return string
     * @throws \Exception
     */
    public function create()
    {
        return View::fetch('form', [
            'data' => null,
            'roles' => json_encode($this->server->getRoleList(HandlerRoleModel::getInstance()))
        ]);
    }

    /**
     * 保存新建的资源
     *
     * @param Request $request
     * @return object
     */
    public function save(Request $request)
    {
        $params = $request->param(['username', 'email', 'mobile', 'password', 'renew_password', 'roles']);

        // 验证请求参数
        $this->validate($params, AdminUser::class . '.create');

        // 额外验证
        if (!isset($params['password']) || !isset($params['renew_password'])) return $this->returnData(500, '密码未设置');

        // 处理
        $result = $this->server->operating(HandlerRoleModel::getInstance(), $params);

        return $this->returnData($result['code'], $result['msg'], $result['data']);
    }

    /**
     * 显示编辑资源表单页
     *
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public function edit(int $id)
    {
        $result = $this->server->resFind($id);

        return View::fetch('form', [
            'data' => $result,
            'roles' => json_encode($this->server->getRoleList(HandlerRoleModel::getInstance()))
        ]);
    }

    /**
     * 保存更新的资源
     *
     * @param Request $request
     * @param int $id
     * @return object
     */
    public function update(Request $request, int $id)
    {
        $params = $request->param(['username', 'email', 'mobile', 'password', 'renew_password', 'roles']);

        $this->validate($params, AdminUser::class . '.edit');

        $result = $this->server->operating(HandlerRoleModel::getInstance(), $params, $id);

        return $this->returnData($result['code'], $result['msg'], $result['data']);
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     * @return object
     */
    public function delete(int $id)
    {
        $result = $this->server->del($id);

        return $this->returnData($result['code'], $result['msg'], $result['data']);
    }
}
