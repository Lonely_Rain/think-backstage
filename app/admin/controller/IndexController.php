<?php
declare (strict_types=1);

namespace app\admin\controller;

use app\admin\service\business\controllers\Index\IndexService;
use think\facade\View;

class IndexController extends BaseController
{
    /** @var IndexService */
    protected $server;

    protected function initialize()
    {
        $this->server = IndexService::getInstance();
    }

    /**
     * 首页
     *
     * @return string
     * @throws \Exception
     */
    public function index()
    {
        return View::fetch('', $this->server->index());
    }

    /**
     * 欢迎页
     *
     * @return string
     * @throws \Exception
     */
    public function welcome()
    {
        return View::fetch('', $this->server->welcome());
    }
}
