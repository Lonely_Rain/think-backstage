<?php
declare (strict_types=1);

namespace app\admin\controller;

use app\admin\service\business\controllers\City\CityService;
use app\admin\validate\City;
use think\facade\View;
use think\Request;

class CityController extends BaseController
{
    /** @var CityService */
    protected $server;

    protected function initialize()
    {
        $this->server = CityService::getInstance();
    }

    /**
     * 显示资源列表
     *
     * @return object|string
     * @throws \Exception
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $params = $this->request->get(['key']);

            return $this->returnData(200, '请求成功', [
                'list' => $this->server->resList($params, $this->request->get('limit', 10, 'intval')),
            ]);
        }

        //
        return View::fetch('', [
            'cityType' => $this->server->type,

            'province' => \app\service\alone\City\CityService::getInstance()->province()
        ]);
    }

    /**
     * 获取市列表
     *
     * @param int $id
     * @param int $type 类型  0 省 1 市 2 区
     * @return \think\Response
     */
    public function getCityList(int $id, int $type)
    {
        $mode = ['province', 'city', 'area'];

        return $this->returnData(200, '请求成功', [
            'list' => call_user_func_array([\app\service\alone\City\CityService::getInstance(), $mode[$type]], $id ? [$id] : [])
        ]);
    }
}
