<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/17
 * Time: 15:20
 */

namespace app\admin\service\specification\token;


interface TokenInterface
{
    /**
     * 获取 tokenAccess
     *
     * @return mixed
     */
    public function get():string;

    /**
     * 获取 token 中 value 值
     *
     * @param string $token
     * @return array
     */
    public function getValue(string $token);

    /**
     * 设置 token
     *
     * @param string $token
     * @param array $value
     * @return mixed
     */
    public function set(string $token, array $value);

    /**
     * 清除 token
     *
     * @param string $token
     * @return mixed
     */
    public function clear(string $token);
}
