<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:46
 */

namespace app\admin\service;


use app\admin\controller\BaseController;
use app\service\traits\Rely;
use app\service\traits\SingleCase;

abstract class BaseService extends BaseController
{
    use SingleCase, Rely;
}
