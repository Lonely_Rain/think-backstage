<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:59
 */

namespace app\admin\service\model;

abstract class BaseModel extends Base
{
    /**
     * 获取列表
     *
     * @param array $where 条件
     * @param array $field 过滤字段
     * @param bool $isPage 是否分页
     * @param int $page 每页条数
     * @return mixed
     */
    abstract public function getList(array $where, array $field = [], bool $isPage = true, int $page = 10);

    /**
     * 获取单条
     *
     * @param int $id 数据id
     * @param array $field 过滤字段
     * @return mixed
     */
    abstract public function getFind(int $id, array $field = []);

    /**
     * 添加
     *
     * @param array $data 数据
     * @return mixed
     */
    abstract public function insert(array $data);

    /**
     * 修改
     *
     * @param array $data 数据
     * @param int $id 数据id
     * @return mixed
     */
    abstract public function edit(array $data, int $id);

    /**
     * 删除
     *
     * @param int $id 数据id
     * @return mixed
     */
    abstract public function delete(int $id);
}
