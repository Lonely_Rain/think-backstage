<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/17
 * Time: 14:37
 */

namespace app\admin\service\model;


use app\admin\service\BaseService;
use app\service\traits\Query;

class Base extends BaseService
{
    use Query;
}
