<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/20
 * Time: 13:38
 */

namespace app\admin\service\business\Alone\Access\Models;


use app\admin\service\model\Base;
use app\model\AdminMenu;
use app\model\AdminUsers;
use think\model\Collection;

class MixedModel extends Base
{
    /**
     * 获取角色列表
     *
     *  规则列表
     *  菜单列表
     *
     * @param AdminUsers $user
     * @return Collection
     */
    public function getRoleList(AdminUsers $user): Collection
    {
        $query = $user->roles()->where('status', 1)->with(['rules' => function ($query) {
            $query->getQuery()->where('status', 1);
        }, 'menus' => function ($query) {
            $query->getQuery()->where('status', 1)->order('sort desc,id asc');
        }]);

        return $this->all($query);
    }

    /**
     * 获取菜单列表
     *
     * @return mixed
     */
    public function getMenuList()
    {
        $query = AdminMenu::where('status', 1)->field(['id', 'title', 'icon', 'uri', 'parent_id'])->order('sort desc,id asc');

        return $this->all($query);
    }
}
