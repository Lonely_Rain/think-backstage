<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/22
 * Time: 15:34
 */

namespace app\admin\service\business\Alone\Config\Models;


use app\admin\service\model\Base;
use app\model\Config;

class ConfigModel extends Base
{
    /**
     * 获取配置信息
     *
     * @param array $keys
     * @return mixed
     */
    public function getConfig(array $keys)
    {
        $query = Config::whereIn('key', $keys)->field(['key', 'value']);

        return $this->all($query, implode('_', $keys));
    }
}
