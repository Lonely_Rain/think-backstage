<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/22
 * Time: 15:33
 */

namespace app\admin\service\business\Alone\Config;


use app\admin\service\BaseService;
use app\admin\service\business\Alone\Config\Models\ConfigModel;

/**
 * Class ConfigService
 *
 * @property-read  ConfigModel $config
 *
 * @package app\admin\service\business\Alone\Config
 */
class ConfigService extends BaseService
{
    protected $rely = [
        'config' => ConfigModel::class
    ];

    /**
     * 获取配置列表
     *
     * @return array
     */
    public function backstage(): array
    {
        $list = $this->config->getConfig(['backstage_title', 'backstage_logo', 'backstage_login_title'])->toArray();

        return arrConversion($list, 'key');
    }
}
