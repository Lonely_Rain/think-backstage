<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/22
 * Time: 13:15
 */

namespace app\admin\service\business\Alone\Nav\Models;


use app\admin\service\model\Base;
use app\model\AdminMenu;

class NavModel extends Base
{
    /**
     * 获取菜单列表
     *
     * @return mixed
     */
    public function getMenuList()
    {
        $query = AdminMenu::field(['id', 'title', 'uri', 'parent_id']);

        return $this->all($query);
    }
}
