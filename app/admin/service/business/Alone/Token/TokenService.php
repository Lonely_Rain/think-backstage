<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/17
 * Time: 15:18
 */

namespace app\admin\service\business\Alone\Token;


use app\admin\service\BaseService;
use app\admin\service\specification\token\TokenInterface;
use think\facade\Session;

class TokenService extends BaseService implements TokenInterface
{

    /**
     * 获取 sessionName
     *
     * @return mixed
     */
    public function get():string
    {
        return config('admin.session.name');
    }

    /**
     * 获取 token 中 value 值
     *
     * @param string $token
     * @return mixed
     */
    public function getValue(string $token)
    {
        return Session::get($token);
    }

    /**
     *
     * 设置 token
     *
     * @param $token
     * @param $value
     * @return mixed
     */
    public function set(string $token, array $value)
    {
        Session::set($token, $value);
    }

    /**
     * 清除 token
     *
     * @param string $token
     * @return mixed
     */
    public function clear(string $token)
    {
        Session::delete($token);
    }
}
