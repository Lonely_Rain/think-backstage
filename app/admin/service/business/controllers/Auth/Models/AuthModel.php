<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/17
 * Time: 14:36
 */

namespace app\admin\service\business\controllers\Auth\Models;


use app\admin\service\model\Base;
use app\model\AdminUsers;

class AuthModel extends Base
{
    /**
     * 获取用户信息
     *
     * @param string $username
     * @return mixed
     */
    public function getUserInfo(string $username){
        $query = AdminUsers::where('username', $username) -> whereOr('email', $username) -> whereOr('mobile', $username) -> field(['id', 'password', 'status']);

        return $this -> one($query);
    }

    /**
     * 权限获取用户信息，通过 id 获取
     *
     * @param int $id
     * @return mixed
     */
    public function authUserInfo(int $id){
        $query = AdminUsers::where('id', $id);

        return $this -> one($query);
    }
}
