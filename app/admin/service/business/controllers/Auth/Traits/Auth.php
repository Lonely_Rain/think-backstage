<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/17
 * Time: 18:17
 */

namespace app\admin\service\business\controllers\Auth\Traits;


trait Auth
{
    /**
     * 设置用户为静态变量
     *
     * @param int $id
     * @return mixed
     */
    public function user(int $id = 0)
    {
        if ($id) self::$user = $this->model->authUserInfo($id);

        return self::$user;
    }
}
