<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/22
 * Time: 21:35
 */

namespace app\admin\service\business\controllers\Article\Articles\Models;


use app\admin\service\model\Base;
use app\model\ArticleCate;

class MixedModel extends Base
{
    /**
     * 获取文章分类列表
     *
     * @return mixed
     */
    public function getArticleCateList()
    {
        $query = ArticleCate::field(['id', 'name']);

        return $this->all($query);
    }

    /**
     * 判断文章分类是否存在
     *
     * @param int $id
     * @return mixed
     */
    public function isArticleCate(int $id)
    {
        $query = ArticleCate::where('id', $id);

        return $this->value($query, 'id');
    }
}
