<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:44
 */

namespace app\admin\service\business\controllers\Article\Articles;


use app\admin\service\business\BaseControllerService;
use app\admin\service\business\controllers\Article\Articles\Models\ArticlesModel;
use app\admin\service\business\controllers\Article\Articles\Models\MixedModel;
use app\service\alone\Path\PathService;

/**
 * Class ArticlesService
 *
 * @property-read ArticlesModel $model
 * @property-read MixedModel $mixed
 * @property-read PathService $path
 *
 * @package app\admin\service\business\controllers\Article\Articles
 */
class ArticlesService extends BaseControllerService
{
    protected $rely = [
        'model' => ArticlesModel::class,
        'mixed' => MixedModel::class,

        'path' => PathService::class
    ];

    /**
     * 查询列表
     *
     * @param array $where
     * @param int $limit
     * @return mixed
     */
    public function resList(array $where, int $limit)
    {
        $list = $this->model->getList($where['key'] ?? [], [], true, $limit);

        // 重新组织结构
        foreach ($list as $k => $v) {
            $list[$k]['cate_name'] = $v->cate->name;

            unset($list[$k]['classify_id'], $list[$k]['cate']);
        }

        return $list;
    }

    /**
     * 查询单条信息
     *
     * @param int $id
     * @return mixed
     */
    public function resFind(int $id)
    {
        $result = $this->model->getFind($id);

        // 处理富文本
        $result->description = $this->path->richTextEncryptionPath(htmlspecialchars_decode($result->description));

        return $result;
    }

    /**
     * 操作数据
     *
     * @param array $params
     * @param int $id
     * @return array
     */
    public function operating(array $params, int $id = 0): array
    {
        // 判断 cate_id 是否存在
        if (!$this->mixed->isArticleCate($params['classify_id'])) return echoArr(500, '请选择文章分类');

        // 判断 name 是否在同一分类下重复
        if ($this->model->isUnique($params['title'], $params['classify_id'], $id)) return echoArr(500, '当前名称在同一分类下已存在');

        // 处理富文本链接
        $descriptionResult = $this->path->richTextPath($params['description']);
        if (200 != $descriptionResult['code']) return $descriptionResult;

        // 需操作的数据
        $data = [
            'title' => $params['title'],
            'classify_id' => $params['classify_id'],
            'description' => htmlspecialchars($descriptionResult['data'])
        ];

        try {
            // 执行数据更新
            if ($id) {
                $this->model->edit($data, $id);
            } else {
                $this->model->insert($data);
            }
        } catch (\Exception $e) {
            return echoArr(800);
        }

        return echoArr(200);
    }


    /**
     * 删除数据
     *
     * @param int $id
     * @return array
     */
    public function del(int $id): array
    {
        try {
            // 删除广告
            $this->model->delete($id);
        } catch (\Exception $exception) {
            return echoArr(800);
        }

        return echoArr(200, '操作成功');
    }

    /**
     * 获取文章分类列表
     *
     * @return mixed
     */
    public function getArticleCateList()
    {
        $list = $this->mixed->getArticleCateList()->toArray();

        foreach ($list as $k => $v) {
            $list[$k]['value'] = $v['id'];

            unset($list[$k]['id']);
        }

        return $list;
    }
}
