<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:58
 */

namespace app\admin\service\business\controllers\Config\Models;


use app\admin\service\model\BaseModel;
use app\model\Config;

class ConfigModel extends BaseModel
{
    /**
     * 获取列表
     *
     * @param array $where
     * @param array $field
     * @param bool $isPage
     * @param int $page
     * @return mixed
     */
    public function getList(array $where, array $field = [], bool $isPage = true, int $page = 10)
    {
        $this->page = $page;
        $this->isPage = $isPage;

        $query = Config::field($field ?: ['id', 'name', 'value', 'value_type as type', 'description', 'create_time']);

        if ($where) {
            // 名称  筛选
            if (isField($where, 'keyWord')) {
                $query->whereLike('name', "%{$where['keyWord']}%");
            }

            // 类型筛选
            if (isField($where, 'type')) {
                $query->where('config_type', $where['type']);
            }
        }

        return $this->all($query);
    }

    /**
     * 获取单条
     *
     * @param int $id
     * @param array $field
     * @return mixed
     */
    public function getFind(int $id, array $field = [])
    {
        $query = Config::where('id', $id)
            ->field($field ?: ['id', 'name', 'value', 'value_type', 'description']);

        return $this->one($query);
    }

    /**
     * 添加
     *
     * @param array $data
     * @return mixed|void
     */
    public function insert(array $data)
    {

    }

    /**
     * 修改
     *
     * @param array $data
     * @param int $id
     * @return mixed
     */
    public function edit(array $data, int $id)
    {
        return $this->update(Config::class, array_merge($data, [
            'id' => $id
        ]));
    }

    /**
     * 删除
     *
     * @param int $id
     * @return mixed|void
     */
    public function delete(int $id)
    {

    }
}
