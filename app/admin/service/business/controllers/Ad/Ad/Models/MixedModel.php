<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/22
 * Time: 21:35
 */

namespace app\admin\service\business\controllers\Ad\Ad\Models;


use app\admin\service\model\Base;
use app\model\AdCate;

class MixedModel extends Base
{
    /**
     * 获取广告分类列表
     *
     * @return mixed
     */
    public function getAdCateList(){
        $query = AdCate::field(['id', 'name']);

        return $this -> all($query);
    }

    /**
     * 判断广告分类是否存在
     *
     * @param int $id
     * @return mixed
     */
    public function isAdCate(int $id){
        $query = AdCate::where('id', $id);

        return $this -> value($query, 'id');
    }
}
