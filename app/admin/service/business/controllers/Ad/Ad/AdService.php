<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:44
 */

namespace app\admin\service\business\controllers\Ad\Ad;


use app\admin\service\business\BaseControllerService;
use app\admin\service\business\controllers\Ad\Ad\Models\AdModel;
use app\admin\service\business\controllers\Ad\Ad\Models\MixedModel;
use app\service\alone\Path\PathService;

/**
 * Class AdService
 *
 * @property-read AdModel $model
 * @property-read MixedModel $mixed
 * @property-read PathService $path
 *
 * @package app\admin\service\business\controllers\Ad\Ad
 */
class AdService extends BaseControllerService
{
    protected $rely = [
        'model' => AdModel::class,
        'mixed' => MixedModel::class,

        'path' => PathService::class
    ];

    /**
     * 查询列表
     *
     * @param array $where
     * @param int $limit
     * @return mixed
     * @throws \Exception
     */
    public function resList(array $where, int $limit)
    {
        $list = $this->model->getList($where['key'] ?? [], [], true, $limit);

        // 重新组织结构
        foreach ($list as $k => $v) {
            $list[$k]['cate_name'] = $v->cate->name;
            $list[$k]['resource'] = $this->path->generateUrl($v->resource)['url'];

            unset($list[$k]['cate_id']);
            unset($list[$k]['cate']);
        }

        return $list;
    }

    /**
     * 查询单条信息
     *
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function resFind(int $id)
    {
        $result = $this->model->getFind($id);

        $result->resource = json_encode($this->path->generateUrl($result->resource));

        return $result;
    }

    /**
     * 操作数据
     *
     * @param array $params
     * @param int $id
     * @return array
     */
    public function operating(array $params, int $id = 0): array
    {
        // 判断 cate_id 是否存在
        if (!$this->mixed->isAdCate($params['cate_id'])) return echoArr(500, '请选择广告分类');

        // 判断 name 是否在同一分类下重复
        if ($this->model->isUnique($params['name'], $params['cate_id'], $id)) return echoArr(500, '当前名称在同一分类下已存在');

        // 判断图片资源
        if (false === $resource = $this->path->fileHandler($params['resource'])) return echoArr(301);

        // 需操作的数据
        $data = [
            'name' => $params['name'],
            'link' => $params['link'],
            'resource' => $resource,
            'cate_id' => $params['cate_id'],
            'description' => $params['description']
        ];

        try {
            // 执行数据更新
            if ($id) {
                $this->model->edit($data, $id);
            } else {
                $this->model->insert($data);
            }
        } catch (\Exception $e) {
            return echoArr(800);
        }

        return echoArr(200);
    }


    /**
     * 删除数据
     *
     * @param int $id
     * @return array
     */
    public function del(int $id): array
    {
        try {
            // 删除广告
            $this->model->delete($id);
        } catch (\Exception $exception) {
            return echoArr(800);
        }

        return echoArr(200, '操作成功');
    }

    /**
     * 获取广告分类列表
     *
     * @return array
     */
    public function getAdCateList(): array
    {
        $list = $this->mixed->getAdCateList()->toArray();

        return $this->handlerSelect($list, 'name', 'id');
    }
}
