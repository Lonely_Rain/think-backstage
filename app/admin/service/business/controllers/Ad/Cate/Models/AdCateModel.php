<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:58
 */

namespace app\admin\service\business\controllers\Ad\Cate\Models;


use app\admin\service\model\BaseModel;
use app\model\AdCate;

class AdCateModel extends BaseModel
{
    /**
     * 获取列表
     *
     * @param array $where
     * @param array $field
     * @param bool $isPage
     * @param int $page
     * @return mixed
     */
    public function getList(array $where, array $field = [], bool $isPage = true, int $page = 10)
    {
        $this -> page = $page;
        $this -> isPage = $isPage;

        $query = AdCate::field($field ?: ['id', 'name', 'description', 'create_time', 'update_time']);

        if($where) {
            // 标题筛选
            if(isField($where, 'name')) {
                $query -> whereLike('name', "%{$where['name']}%");
            };

            // 时间筛选
            if(isField($where, 'start') && isField($where, 'end')) {
                $query -> whereBetweenTime('create_time', $where['start'], $where['end']);
            }
        }

        return $this -> all($query);
    }

    /**
     * 获取单条
     *
     * @param int $id
     * @param array $field
     * @return mixed
     */
    public function getFind(int $id, array $field = [])
    {
        $query = AdCate::where('id', $id)
                -> field($field ?: ['id', 'name', 'description']);

        return $this -> one($query);
    }

    /**
     * 添加
     *
     * @param array $data
     * @return mixed
     */
    public function insert(array $data)
    {
        return $this -> add(AdCate::class, $data);
    }

    /**
     * 修改
     *
     * @param array $data
     * @param int $id
     * @return mixed
     */
    public function edit(array $data, int $id)
    {
        return $this -> update(AdCate::class, array_merge($data, [
            'id' => $id
        ]));
    }

    /**
     * 删除
     *
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this -> del(AdCate::class, [ $id ]);
    }

    /**
     * 标题是否重复
     *
     * @param string $name
     * @param int $id
     * @return mixed
     */
    public function isUnique(string $name, int $id){
        $query = AdCate::where('id', '<>', $id)
                -> where('name', $name);

        return $this -> value($query, 'id');
    }

    /**
     * 判断当前分类是否有广告
     *
     * @param int $id
     * @return mixed
     */
    public function isAd(int $id){
        $query = AdCate::where('id', $id) -> with('ad');

        $adCate = $this -> one($query);

        return $this -> value($adCate -> ad(), 'id');
    }
}
