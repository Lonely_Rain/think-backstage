<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:58
 */

namespace app\admin\service\business\controllers\Access\Rule\Models;


use app\admin\service\model\BaseModel;
use app\model\AdminRule;

class AdminRuleModel extends BaseModel
{
    /**
     * 获取列表
     *
     * @param array $where
     * @param array $field
     * @param bool $isPage
     * @param int $page
     * @return mixed
     */
    public function getList(array $where, array $field = [], bool $isPage = true, int $page = 10)
    {
        $this -> page = $page;
        $this -> isPage = $isPage;

        $query = AdminRule::field($field ?: ['id', 'name', 'method', 'routes', 'status', 'create_time', 'update_time']);

        if($where) {
            // 路由名称筛选
            if(isField($where, 'name')) {
                $query -> whereLike('name|routes', "%{$where['name']}%");
            };

            // 判断请求方式
            if(isField($where, 'method')){
                $query -> where('method', $where['method']);
            }

            // 时间筛选
            if(isField($where, 'start') && isField($where, 'end')) {
                $query -> whereBetweenTime('create_time', $where['start'], $where['end']);
            }

            // 状态筛选
            if(isField($where, 'status')){
                $query -> where('status', $where['status']);
            }
        }

        return $this -> all($query);
    }

    /**
     * 获取单条
     *
     * @param int $id
     * @param array $field
     * @return mixed
     */
    public function getFind(int $id, array $field = [])
    {
        $query = AdminRule::where('id', $id) -> field($field ?: ['id', 'name', 'method', 'routes']);

        return $this -> one($query);
    }

    /**
     * 添加
     *
     * @param array $data
     * @return mixed
     */
    public function insert(array $data)
    {
        return $this -> add(AdminRule::class, $data);
    }

    /**
     * 修改
     *
     * @param array $data
     * @param int $id
     * @return mixed
     */
    public function edit(array $data, int $id)
    {
        return $this -> update(AdminRule::class, array_merge($data, [
            'id' => $id
        ]));
    }

    /**
     * 删除
     *
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this -> del(AdminRule::class, [ $id ]);
    }

    /**
     * 名称是否重复
     *
     * @param string $name
     * @param string $method
     * @param int $id
     * @return mixed
     */
    public function isUnique(string $name, string $method, int $id){
        $query = AdminRule::where('id', '<>', $id)
                -> where('name', $name)
                -> where('method', $method);

        return $this -> value($query, 'id');
    }

    /**
     * 是否有角色拥有此权限
     *
     * @param int $id
     * @return bool
     */
    public function isRoleRule(int $id){
        $query = AdminRule::where('id', $id) -> with('roles');

        $rule = $this -> one($query);

        return !$rule -> roles -> isEmpty();
    }
}
