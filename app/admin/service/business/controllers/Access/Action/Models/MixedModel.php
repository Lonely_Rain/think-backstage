<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/26
 * Time: 14:01
 */

namespace app\admin\service\business\controllers\Access\Action\Models;


use app\admin\service\model\Base;
use app\model\AdminUsers;

class MixedModel extends Base
{
    /**
     * 获取管理员列表
     *
     * @return mixed
     */
    public function getAdminList()
    {
        $query = AdminUsers::field(['id', 'username']);

        return $this->all($query);
    }
}
