<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:58
 */

namespace app\admin\service\business\controllers\Access\Action\Models;


use app\admin\service\model\BaseModel;
use app\model\AdminActionLog;

class ActionModel extends BaseModel
{
    /**
     * 获取列表
     *
     * @param array $where
     * @param array $field
     * @param bool $isPage
     * @param int $page
     * @return mixed
     */
    public function getList(array $where, array $field = [], bool $isPage = true, int $page = 10)
    {
        $this->page = $page;
        $this->isPage = $isPage;

        $query = AdminActionLog::field($field ?: ['id', 'user_id', 'path', 'method', 'ip', 'input', 'create_time'])
            ->with(['admin' => function ($query) {
                $query->field(['id', 'username']);
            }])->order(['id desc']);

        if ($where) {
            // 用户筛选
            if (isField($where, 'user_id')) {
                $query->where('user_id', $where['user_id']);
            }

            // uri 或 ip 搜索
            if (isField($where, 'keyWord')) {
                $query->whereLike('path|ip', "%{$where['keyWord']}%");
            }

            // 请求方式筛选
            if (isField($where, 'method')) {
                $query->where('method', $where['method']);
            }
        }

        return $this->all($query);
    }

    /**
     * 获取单条
     *
     * @param int $id
     * @param array $field
     * @return mixed|void
     */
    public function getFind(int $id, array $field = [])
    {

    }

    /**
     * 添加
     *
     * @param array $data
     * @return mixed
     */
    public function insert(array $data)
    {
        return $this->add(AdminActionLog::class, $data);
    }

    /**
     * 修改
     *
     * @param array $data
     * @param int $id
     * @return mixed|void
     */
    public function edit(array $data, int $id)
    {

    }

    /**
     * 删除
     *
     * @param int $id
     * @return mixed|void
     */
    public function delete(int $id)
    {

    }
}
