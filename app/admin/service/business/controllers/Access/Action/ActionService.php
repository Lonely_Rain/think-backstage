<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:44
 */

namespace app\admin\service\business\controllers\Access\Action;


use app\admin\service\business\BaseControllerService;
use app\admin\service\business\controllers\Access\Achieve\Traits\Additional;
use app\admin\service\business\controllers\Access\Action\Models\ActionModel;
use app\admin\service\business\controllers\Access\Action\Models\MixedModel;

/**
 * Class ActionService
 *
 * @property-read ActionModel $model
 * @property-read MixedModel $mixed
 *
 * @package app\admin\service\business\controllers\Access\Action
 */
class ActionService extends BaseControllerService
{
    use Additional;

    protected $rely = [
        'model' => ActionModel::class,

        'mixed' => MixedModel::class,
    ];

    /**
     * 查询列表
     *
     * @param array $where
     * @param int $limit
     * @return mixed
     * @throws \think\Exception
     */
    public function resList(array $where, int $limit)
    {
        $list = $this->model->getList($where['key'] ?? [], [], true, $limit);

        foreach ($list as $k => $v) {
            $list[$k]['username'] = $v->admin ? $v->admin->username : '';
        }

        return $list;
    }

    /**
     * 获取管理员列表
     *
     * @return mixed
     * @throws \think\Exception
     */
    public function getAdminList()
    {
        return $this->mixed->getAdminList();
    }

    /**
     * 添加日志
     *
     * @param array $data
     * @return array
     */
    public function operating(array $data = []): array
    {
        $data = $data ?: [
            'user_id' => self::$user->id,
            'path' => request()->rule()->getRule() ?: '/',
            'method' => request()->method(),
            'ip' => request()->ip(),
            'input' => json_encode(request()->param())
        ];

        try {
            $this->model->insert($data);

            return echoArr(200);
        } catch (\Exception $exception) {
            return echoArr(800);
        }
    }
}
