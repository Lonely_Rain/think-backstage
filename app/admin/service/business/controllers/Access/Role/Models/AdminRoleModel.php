<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:58
 */

namespace app\admin\service\business\controllers\Access\Role\Models;


use app\admin\service\model\BaseModel;
use app\model\AdminRole;

class AdminRoleModel extends BaseModel
{
    /**
     * 获取列表
     *
     * @param array $where
     * @param array $field
     * @param bool $isPage
     * @param int $page
     * @return mixed
     */
    public function getList(array $where, array $field = [], bool $isPage = true, int $page = 10)
    {
        $this -> page = $page;
        $this -> isPage = $isPage;

        $query = AdminRole::field($field ?: ['id', 'name', 'description', 'status', 'create_time', 'update_time']);

        if($where) {
            // 名称筛选
            if(isField($where, 'name')) {
                $query -> whereLike('name', "%{$where['name']}%");
            };

            // 时间筛选
            if(isField($where, 'start') && isField($where, 'end')) {
                $query -> whereBetweenTime('create_time', $where['start'], $where['end']);
            }

            // 状态筛选
            if(isField($where, 'status')){
                $query -> where('status', $where['status']);
            }
        }

        return $this -> all($query);
    }

    /**
     * 获取单条
     *
     * @param int $id
     * @param array $field
     * @return mixed
     */
    public function getFind(int $id, array $field = [])
    {
        $query = AdminRole::where('id', $id)
                -> field($field ?: ['id', 'name', 'description'])
                -> with('rules');

        return $this -> one($query);
    }

    /**
     * 添加
     *
     * @param array $data
     * @return mixed
     */
    public function insert(array $data)
    {
        return $this -> add(AdminRole::class, $data);
    }

    /**
     * 修改
     *
     * @param array $data
     * @param int $id
     * @return mixed
     */
    public function edit(array $data, int $id)
    {
        return $this -> update(AdminRole::class, array_merge($data, [
            'id' => $id
        ]));
    }

    /**
     * 删除
     *
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this -> del(AdminRole::class, [ $id ]);
    }

    /**
     * 名称是否重复
     *
     * @param string $name
     * @param int $id
     * @return mixed
     */
    public function isUnique(string $name, int $id){
        $query = AdminRole::where('id', '<>', $id)
                -> where('name', $name);

        return $this -> value($query, 'id');
    }

    /**
     * 更新权限
     *
     * @param int $roleId
     * @param array $ids
     */
    public function updateRule(int $roleId, array $ids){
        $role = $this -> one(AdminRole::where('id', $roleId));

        // 清空当前角色权限
        $role -> rules() -> detach();

        // 新增权限
        if($ids) $role -> rules() -> saveAll($ids);
    }

    /**
     * 是否有管理员拥有此角色
     *
     * @param int $id
     * @return bool
     */
    public function isAdminRole(int $id){
        $query = AdminRole::where('id', $id) -> with('users');

        $role = $this -> one($query);

        return !$role -> users -> isEmpty();
    }

    /**
     * 是否有菜单拥有此角色
     *
     * @param int $id
     * @return bool
     */
    public function isMenuRole(int $id){
        $query = AdminRole::where('id', $id) -> with('menus');

        $role = $this -> one($query);

        return !$role -> menus -> isEmpty();
    }
}
