<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/19
 * Time: 0:53
 */

namespace app\admin\service\business\controllers\Access\Role\Models;


use app\admin\service\model\Base;
use app\model\AdminRule;

class MixedModel extends Base
{
    /**
     * 获取权限列表
     *
     * @return mixed
     */
    public function getRuleList()
    {
        $query = AdminRule::field(['id', 'name', 'method']);

        return $this->all($query);
    }


    /**
     * 获取权限id集合
     *
     * @param array $ids
     * @return array
     */
    public function getRuleIds(array $ids): array
    {
        $query = AdminRule::whereIn('id', $ids);

        return $this -> column($query, 'id');
    }
}
