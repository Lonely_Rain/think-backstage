<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/20
 * Time: 9:54
 */

namespace app\admin\service\business\controllers\Access\Interfaces;


interface HandlerRoleModelInterface
{
    /**
     * 获取角色列表
     *
     * @return mixed
     */
    public function getRoleList();

    /**
     * 根据条件重新获取角色id
     *
     * @param array $ids    角色 id 集合
     * @return array
     */
    public function getRoleIds(array $ids): array;
}
