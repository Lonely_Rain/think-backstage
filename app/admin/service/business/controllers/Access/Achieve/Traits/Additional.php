<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/18
 * Time: 22:07
 */

namespace app\admin\service\business\controllers\Access\Achieve\Traits;


trait Additional
{
    /**
     * 请求方式
     *
     * @return array
     */
    public function methods(){
        return [
            'ANY', 'GET', 'POST', 'PUT', 'DELETE'
        ];
    }
}
