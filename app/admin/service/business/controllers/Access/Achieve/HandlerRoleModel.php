<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/20
 * Time: 9:58
 */

namespace app\admin\service\business\controllers\Access\Achieve;


use app\admin\service\business\controllers\Access\Interfaces\HandlerRoleModelInterface;
use app\admin\service\model\Base;
use app\model\AdminRole;

class HandlerRoleModel extends Base implements HandlerRoleModelInterface
{
    /**
     * 获取权限列表
     *
     * @return mixed
     */
    public function getRoleList()
    {
        $query = AdminRole::field(['id', 'name']);

        return $this->all($query);
    }

    /**
     * 获取角色id集合
     *
     * @param array $ids
     * @return array
     */
    public function getRoleIds(array $ids): array
    {
        $query = AdminRole::whereIn('id', $ids);

        return $this->column($query, 'id');
    }
}
