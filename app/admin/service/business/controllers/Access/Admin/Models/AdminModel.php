<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:58
 */

namespace app\admin\service\business\controllers\Access\Admin\Models;


use app\admin\service\model\BaseModel;
use app\model\AdminUsers;

class AdminModel extends BaseModel
{
    /**
     * 获取列表
     *
     * @param array $where
     * @param array $field
     * @param bool $isPage
     * @param int $page
     * @return mixed
     */
    public function getList(array $where, array $field = [], bool $isPage = true, int $page = 10)
    {
        $this -> page = $page;
        $this -> isPage = $isPage;

        $query = AdminUsers::field($field ?: ['id', 'username', 'email', 'mobile', 'status', 'create_time', 'update_time']);

        if($where) {
            // 用户|邮箱|手机号  筛选
            if(isField($where, 'keyWord')) {
                $query -> whereLike('username|email|mobile', "%{$where['keyWord']}%");
            }

            // 时间筛选
            if(isField($where, 'start') && isField($where, 'end')) {
                $query -> whereBetweenTime('create_time', $where['start'], $where['end']);
            }
        }

        return $this -> all($query);
    }

    /**
     * 获取单条
     *
     * @param int $id
     * @param array $field
     * @return mixed
     */
    public function getFind(int $id, array $field = [])
    {
        $query = AdminUsers::where('id', $id)
                -> field($field ?: ['id', 'username', 'email', 'mobile'])
                -> with(['roles']);

        return $this -> one($query);
    }

    /**
     * 添加
     *
     * @param array $data
     * @return mixed
     */
    public function insert(array $data)
    {
        return $this -> add(AdminUsers::class, $data);
    }

    /**
     * 修改
     *
     * @param array $data
     * @param int $id
     * @return mixed
     */
    public function edit(array $data, int $id)
    {
        return $this -> update(AdminUsers::class, array_merge($data, [
            'id' => $id
        ]));
    }

    /**
     * 删除
     *
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this -> del(AdminUsers::class, [ $id ]);
    }

    /**
     * 名称是否重复
     *
     * @param array $where
     * @param int $id
     * @return mixed
     */
    public function isUnique(array $where, int $id){
        $query = AdminUsers::where('id', '<>', $id) -> where(function ($query) use ($where){
            $query -> where('username', 'like', "%{$where['username']}%")
                -> whereOr('mobile', 'like', "%{$where['mobile']}%")
                -> whereOr('email', 'like', "%{$where['email']}%");
        });

        return $this -> value($query, 'id');
    }

    /**
     * 更新角色
     *
     * @param int $userId
     * @param array $ids
     */
    public function updateRole(int $userId, array $ids){
        $user = $this -> one(AdminUsers::where('id', $userId));

        // 清空当前角色权限
        $user -> roles() -> detach();

        // 新增权限
        if($ids) $user -> roles() -> saveAll($ids);
    }
}
