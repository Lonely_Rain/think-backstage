<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:44
 */

namespace app\admin\service\business\controllers\Access\Admin;


use app\admin\service\business\BaseControllerService;
use app\admin\service\business\controllers\Access\Admin\Models\AdminModel;
use app\admin\service\business\controllers\Access\Interfaces\HandlerRoleModelInterface;
use think\facade\Db;

/**
 * Class AdminService
 *
 * @property-read AdminModel $model
 *
 * @package app\admin\service\business\controllers\Access\Admin
 */
class AdminService extends BaseControllerService
{
    protected $rely = [
        'model' => AdminModel::class,
    ];

    /**
     * 查询列表
     *
     * @param array $where
     * @param int $limit
     * @return mixed
     */
    public function resList(array $where, int $limit)
    {
        $list = $this->model->getList($where['key'] ?? [], [], true, $limit);

        return $list;
    }

    /**
     * 查询单条信息
     *
     * @param int $id
     * @return mixed
     */
    public function resFind(int $id)
    {
        $result = $this->model->getFind($id);

        // 角色id
        $result -> roleIds = implode(',', $result -> roles -> column('id'));

        return $result;
    }

    /**
     * 操作数据
     *
     * @param HandlerRoleModelInterface $handlerRoleModel
     * @param array $params
     * @param int $id
     * @return array
     */
    public function operating(HandlerRoleModelInterface $handlerRoleModel, array $params, int $id = 0): array
    {
        // 编辑除超级管理员必须选择角色
        if(1 != $id && (!isset($params['roles']) || !$params['roles'])) return echoArr(500, '请选择角色');

        // 不允许当前管理员修改或创建其他管理员，除超级管理员
        if(1 != self::$user -> id) {
            if($id && self::$user -> id != $id) return echoArr(610);

            if(!$id) return echoArr(611);
        }

        // 判断 username|mobile|email 是否重复
        if ($this->model->isUnique([
            'username' => $params['username'],
            'mobile' => $params['mobile'],
            'email' => $params['email']
        ], $id)) return echoArr(500, '名称|手机号|邮箱有重复');

        // 处理角色
        if(1 != $id) if(!$roleIds = $handlerRoleModel -> getRoleIds(explode(',', $params['roles']))) return echoArr(500, '请选择角色');

        // 需操作的数据
        $data = [
            'username' => $params['username'],
            'mobile' => $params['mobile'],
            'email' => $params['email'],
        ];

        // 密码处理
        if(isset($params['password']) && trim($params['password'])) {
            // 判断密码是否一致
            if($params['password'] !== $params['renew_password']) return echoArr(500, '密码不一致');

            // 密码加密
            $newPassword = md5(config('admin.web.key') . trim($params['password']));

            $data['password'] = sha1($newPassword . config('app.key'));
        }

        // 开启事务
        Db::startTrans();

        try {
            // 执行数据更新
            if ($id) {
                $result = $this->model->edit($data, $id);
            } else {
                $result = $this->model->insert($data);
            }

            //  获取用户id
            if(!$id) $id = $result -> id;

            // 更新角色
            if(1 != $id) $this -> model -> updateRole($id, $roleIds);

            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();

            return echoArr(800);
        }

        return echoArr(200);
    }


    /**
     * 删除数据
     *
     * @param int $id
     * @return array
     */
    public function del(int $id): array
    {
        // 超级管理员不允许删除
        if(1 == $id) return echoArr(800);

        // 开启事务
        Db::startTrans();

        try {
            // 更新角色
            $this -> model -> updateRole($id, []);

            // 删除角色
            $this -> model -> delete($id);

            Db::commit();
        } catch (\Exception $exception) {
            Db::rollback();

            return echoArr(800);
        }

        return echoArr(200, '操作成功');
    }

    /**
     * 获取权限列表
     *
     * @param HandlerRoleModelInterface $handlerRoleModel
     * @return mixed
     */
    public function getRoleList(HandlerRoleModelInterface $handlerRoleModel){
        return $handlerRoleModel -> getRoleList() -> toArray();
    }

    /**
     * 处理修改单个值逻辑
     *
     * @param int $id
     * @param array $data
     * @param int $mode
     * @return array
     */
    public function handlerIndividualValue(int $id, array $data, int $mode){
        if(1 != self::$user -> id || 1 == $id) return echoArr(612);

        return echoArr(200);
    }
}
