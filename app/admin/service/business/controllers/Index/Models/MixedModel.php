<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/20
 * Time: 23:36
 */

namespace app\admin\service\business\controllers\Index\Models;


use app\admin\service\model\Base;
use app\model\AdminUsers;

class MixedModel extends Base
{
    /**
     * 获取管理员总数
     *
     * @return int
     */
    public function getAdminCount(){
        return AdminUsers::where(1) -> count();
    }
}
