<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/20
 * Time: 23:00
 */

namespace app\admin\service\business\controllers\Index;


use app\admin\service\business\Alone\Access\AccessMenuService;
use app\admin\service\business\Alone\Access\AccessService;
use app\admin\service\business\BaseControllerService;
use app\admin\service\business\controllers\Index\Models\MixedModel;
use think\facade\Db;

/**
 * Class IndexService
 *
 * @property-read AccessService $access
 * @property-read MixedModel $mixed
 *
 * @package app\admin\service\business\controllers\Index
 */
class IndexService extends BaseControllerService
{
    protected $rely = [
        'access' => AccessService::class,

        'mixed' => MixedModel::class
    ];

    /**
     * 首页逻辑处理
     *
     * @return array
     * @throws \think\Exception
     */
    public function index(): array
    {
        $menus = AccessMenuService::getInstance(['roles' => $this->access->roles])->boot($this->access->isAdmin());

        return [
            'column_menu' => json_encode($menus),
            'user' => [
                'username' => self::$user->username,
            ],
        ];
    }

    /**
     * 欢迎页逻辑处理
     *
     * @return array
     */
    public function welcome(): array
    {
        // 服务器信息
        $server = [
            // 获取当前域名
            [
                'name' => '服务器域名',
                'value' => request()->domain(),
            ],

            // 获取当前系统
            [
                'name' => '操作系统',
                'value' => php_uname(),
            ],

            // 获取服务器地址
            [
                'name' => '服务器地址',
                'value' => request()->ip(),
            ],

            // 获取服务器端口
            [
                'name' => '服务器端口',
                'value' => request()->port(),
            ],

            // 获取当前环境
            [
                'name' => '服务器环境',
                'value' => request()->server('SERVER_SOFTWARE'),
            ],

            // 获取php当前版本
            [
                'name' => 'PHP版本',
                'value' => PHP_VERSION,
            ],

            // 获取php当前运行方式
            [
                'name' => 'PHP运行方式',
                'value' => php_sapi_name(),
            ],

            // 上传文件最大限制
            [
                'name' => '上传附件限制',
                'value' => get_cfg_var("upload_max_filesize") ?: "不允许",
            ],

            // 执行时间限制
            [
                'name' => '执行时间限制',
                'value' => get_cfg_var("max_execution_time") . "秒",
            ],

            // 数据库版本
            [
                'name' => '数据库版本',
                'value' => Db::query("select version() as ver")[0]['ver'],
            ],
        ];

        // 记录总数
        $count = [
            [
                'name' => '管理员数',
                'value' => $this->mixed->getAdminCount()
            ]
        ];

        return [
            'server' => $server,

            'count' => $count,

            'user' => [
                'username' => self::$user->username
            ]
        ];
    }
}
