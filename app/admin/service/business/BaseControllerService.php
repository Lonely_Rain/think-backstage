<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/16
 * Time: 13:47
 */

namespace app\admin\service\business;


abstract class BaseControllerService extends \app\admin\service\BaseService
{
    // 用户
    static protected $user;

    /**
     * 处理单个值
     *
     * 例如：状态，排序等
     *
     * @param int $id
     * @param array $data
     * @param int $mode
     * @return array
     */
    public function editIndividualValue(int $id, array $data, int $mode)
    {
        // 逻辑处理
        if (method_exists($this, 'handlerIndividualValue')) {
            $result = $this->handlerIndividualValue($id, $data, $mode);

            if (200 != $result['code']) return $result;
        }

        // 数据结构是否需要调整
        switch ($mode) {
            // 状态
            case 1:
                if (method_exists($this, 'getStatus')) $data = $this->getStatus($data);
                break;
            // 排序
            case 2:
                if (method_exists($this, 'getSort')) $data = $this->getSort($data);
                break;
            // 审核
            case 3:
                if (method_exists($this, 'getReview')) $data = $this->getReview($data);
        }

        // 更新数据
        if(false === $this->model->edit($data, $id)) return echoArr(800);

        return echoArr(200);
    }

    /**
     * 处理格式为 select 所需格式
     *
     * @param array $list
     * @param string $name
     * @param string $value
     * @return array
     */
    protected function handlerSelect(array $list, string $name = 'name', string $value = 'id'): array
    {
        return array_reduce($list, function ($result, $v) use ($name, $value) {
            $result[] = [
                'name' => $v[$name],
                'value' => $v[$value]
            ];

            return $result;
        }, []);
    }
}
