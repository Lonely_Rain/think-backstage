<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// 登录
Route::rule('login', 'Auth/login', 'GET|POST')->name('login')->token();

// 换取 token
Route::post('exchangeToken', 'Auth/exchange')->name('exchangeToken');

// 404 页面
Route::get('404', function () {
    return view('layout/error');
})->name('404');

// 权限控制
Route::group('', function () {
    // 首页
    Route::get('/', 'Index/index')->name('index');
    Route::get('welcome', 'Index/welcome')->name('welcome');

    // 需验证 token 令牌
    Route::group('', function () {
        // 退出登录
        Route::post('logout', 'Auth/logout')->name('logout');

        // 管理员模块
        Route::resource('admin-user', 'Admin')->except(['read']);
        Route::put('admin-user/:id/status', 'Admin/status')->name('admin-user.status');

        // 权限模块
        Route::resource('admin-rule', 'AdminRule')->except(['read']);
        Route::put('admin-rule/:id/status', 'AdminRule/status')->name('admin-rule.status');

        // 角色模块
        Route::resource('admin-role', 'AdminRole')->except(['read']);
        Route::put('admin-role/:id/status', 'AdminRole/status')->name('admin-role.status');

        // 权限菜单
        Route::resource('admin-menu', 'AdminMenu')->except(['read']);
        Route::put('admin-menu/:id/status', 'AdminMenu/status')->name('admin-menu.status');
        Route::put('admin-menu/:id/sort', 'AdminMenu/sort')->name('admin-menu.sort');

        // 操作日志
        Route::resource('action-log', 'AdminActionLog')->only(['index']);

        // 配置信息
        Route::resource('config-base', 'Config')->except(['read', 'create', 'delete']);
        Route::resource('config-web', 'Config')->except(['read', 'create', 'delete']);

        // 地区管理
        Route::resource('city', 'City')->only(['index']);
        Route::put('city/:id/sort', 'City/sort')->name('city.sort');
        Route::put('city/:id/status', 'City/status')->name('city.status');
        Route::get('city/:id/get-city-list/:type', 'City/getCityList')->name('city.get.city-list');

        // 广告管理
        Route::resource('ad-cate', 'AdCate')->except(['read']);
        Route::resource('ad-list', 'Ad')->except(['read']);
        Route::put('ad-list/:id/status', 'Ad/status')->name('ad-list.status');
        Route::put('ad-list/:id/target', 'Ad/target')->name('ad-list.target');
        Route::put('ad-list/:id/sort', 'Ad/sort')->name('ad-list.sort');

        // 文章分类
        Route::resource('article-cate', 'ArticleCate')->except(['read']);
        Route::put('article-cate/:id/status', 'ArticleCate/status')->name('article-cate.status');
        Route::put('article-cate/:id/sort', 'ArticleCate/sort')->name('article-cate.sort');

        // 文章列表
        Route::resource('article', 'Articles')->except(['read']);
        Route::put('article/:id/status', 'Articles/status')->name('article.status');
        Route::put('article/:id/sort', 'Articles/sort')->name('article.sort');

        // 上传资源
        Route::post('upload-img', 'Upload/img')->name('upload.img');
        Route::post('upload-video', 'Upload/video')->name('upload.video');
        Route::post('upload-file', 'Upload/file')->name('upload.file');

        // 文件访问
        Route::get('file/:input', 'app\resource\FileController/handler')->name('admin-file');
    })->token();

    // 多图上传
    Route::post('upload-file-img', 'Upload/img')->name('upload.file.img');

    // 多视频上传
    Route::post('upload-file-video', 'Upload/video')->name('upload.file.video');

    // 多文件上传
    Route::post('upload-file-file', 'Upload/file')->name('upload.file.file');

    // 文件分片上传 - （获取文件上传key、文件块上传、重置文件块、取消上传）
    Route::rule('multipart-upload/:action', 'Upload/multipartUpload', 'GET|POST')->name('upload.multipart-upload');
})->middleware(['CheckAuth', 'Nav', 'ActionLog']);


