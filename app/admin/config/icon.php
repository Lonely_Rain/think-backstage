<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/15
 * Time: 22:01
 */

return [
    'x-admin' => [
        [
            'name' => 'all',
            'code' => '&#xe696;'
        ],

        [
            'name' => 'back',
            'code' => '&#xe697;'
        ],

        [
            'name' => 'Category',
            'code' => '&#xe699;'
        ],

        [
            'name' => 'close',
            'code' => '&#xe69a;'
        ],

        [
            'name' => 'comments',
            'code' => '&#xe69b;'
        ],

        [
            'name' => 'cry',
            'code' => '&#xe69c;'
        ],

        [
            'name' => 'delete',
            'code' => '&#xe69d;'
        ],

        [
            'name' => 'edit',
            'code' => '&#xe69e;'
        ],

        [
            'name' => 'email',
            'code' => '&#xe69f;'
        ],

        [
            'name' => 'favorite',
            'code' => '&#xe6a0;'
        ],

        [
            'name' => 'form',
            'code' => '&#xe6a2;'
        ],

        [
            'name' => 'help',
            'code' => '&#xe6a3;'
        ],

        [
            'name' => 'information',
            'code' => '&#xe6a4;'
        ],

        [
            'name' => 'less',
            'code' => '&#xe6a5;'
        ],

        [
            'name' => 'more_unfold',
            'code' => '&#xe6a6;'
        ],

        [
            'name' => 'more',
            'code' => '&#xe6a7;'
        ],

        [
            'name' => 'pic',
            'code' => '&#xe6a8;'
        ],

        [
            'name' => 'QRCode',
            'code' => '&#xe6a9;'
        ],

        [
            'name' => 'refresh',
            'code' => '&#xe6aa;'
        ],

        [
            'name' => 'RFQ',
            'code' => '&#xe6ab;'
        ],

        [
            'name' => 'search',
            'code' => '&#xe6ac;'
        ],

        [
            'name' => 'selected',
            'code' => '&#xe6ad;'
        ],

        [
            'name' => 'set',
            'code' => '&#xe6ae;'
        ],

        [
            'name' => 'Smile',
            'code' => '&#xe6af;'
        ],

        [
            'name' => 'success',
            'code' => '&#xe6b1;'
        ],

        [
            'name' => 'survey',
            'code' => '&#xe6b2;'
        ],

        [
            'name' => 'training',
            'code' => '&#xe6b3;'
        ],

        [
            'name' => 'ViewGallery',
            'code' => '&#xe6b4;'
        ],

        [
            'name' => 'Viewlist',
            'code' => '&#xe6b5;'
        ],

        [
            'name' => 'warning',
            'code' => '&#xe6b6;'
        ],

        [
            'name' => 'wrong',
            'code' => '&#xe6b7;'
        ],

        [
            'name' => 'account',
            'code' => '&#xe6b8;'
        ],

        [
            'name' => 'add',
            'code' => '&#xe6b9;'
        ],

        [
            'name' => 'atm',
            'code' => '&#xe6ba;'
        ],

        [
            'name' => 'clock',
            'code' => '&#xe6bb;'
        ],

        [
            'name' => 'remind',
            'code' => '&#xe6bc;'
        ],

        [
            'name' => 'calendar',
            'code' => '&#xe6bf;'
        ],

        [
            'name' => 'attachment',
            'code' => '&#xe6c0;'
        ],

        [
            'name' => 'discount',
            'code' => '&#xe6c5;'
        ],

        [
            'name' => 'service',
            'code' => '&#xe6c7;'
        ],

        [
            'name' => 'print',
            'code' => '&#xe6c9;'
        ],

        [
            'name' => 'box',
            'code' => '&#xe6cb;'
        ],

        [
            'name' => 'process',
            'code' => '&#xe6ce;'
        ],

        [
            'name' => 'beauty',
            'code' => '&#xe6d2;'
        ],

        [
            'name' => 'electrical',
            'code' => '&#xe6d4;'
        ],

        [
            'name' => 'home',
            'code' => '&#xe6d7;'
        ],

        [
            'name' => 'electronics',
            'code' => '&#xe6da;'
        ],

        [
            'name' => 'gifts',
            'code' => '&#xe6db;'
        ],

        [
            'name' => 'lights',
            'code' => '&#xe6de;'
        ],

        [
            'name' => 'sports',
            'code' => '&#xe6e0;'
        ],

        [
            'name' => 'toys',
            'code' => '&#xe6e1;'
        ],

        [
            'name' => 'auto',
            'code' => '&#xe6e3;'
        ],

        [
            'name' => 'jewelry',
            'code' => '&#xe6e4;'
        ],

        [
            'name' => 'trade-assurance',
            'code' => '&#xe6e5;'
        ],

        [
            'name' => 'browse',
            'code' => '&#xe6e6;'
        ],

        [
            'name' => 'rfq-qm',
            'code' => '&#xe6e7;'
        ],

        [
            'name' => 'rfq-quantity',
            'code' => '&#xe6e8;'
        ],

        [
            'name' => 'atm-away',
            'code' => '&#xe6e9;'
        ],

        [
            'name' => 'rfq',
            'code' => '&#xe6eb;'
        ],

        [
            'name' => 'scanning',
            'code' => '&#xe6ec;'
        ],

        [
            'name' => 'compare',
            'code' => '&#xe6ee;'
        ],

        [
            'name' => 'filter',
            'code' => '&#xe6f1;'
        ],

        [
            'name' => 'pin',
            'code' => '&#xe6f2;'
        ],

        [
            'name' => 'history',
            'code' => '&#xe6f3;'
        ],

        [
            'name' => 'product-features',
            'code' => '&#xe6f4;'
        ],

        [
            'name' => 'supplier-features',
            'code' => '&#xe6f5;'
        ],

        [
            'name' => 'similar-product',
            'code' => '&#xe6f6;'
        ],

        [
            'name' => 'link',
            'code' => '&#xe6f7;'
        ],

        [
            'name' => 'cut',
            'code' => '&#xe6f8;'
        ],

        [
            'name' => 'nav-list',
            'code' => '&#xe6fa;'
        ],

        [
            'name' => 'image-text',
            'code' => '&#xe6fb;'
        ],

        [
            'name' => 'text',
            'code' => '&#xe6fc;'
        ],

        [
            'name' => 'move',
            'code' => '&#xe6fd;'
        ],

        [
            'name' => 'subtract',
            'code' => '&#xe6fe;'
        ],

        [
            'name' => 'dollar',
            'code' => '&#xe702;'
        ],

        [
            'name' => 'raw',
            'code' => '&#xe704;'
        ],

        [
            'name' => 'office',
            'code' => '&#xe705;'
        ],

        [
            'name' => 'agriculture',
            'code' => '&#xe707;'
        ],

        [
            'name' => 'machinery',
            'code' => '&#xe709;'
        ],

        [
            'name' => 'assessed-Badge',
            'code' => '&#xe70a;'
        ],

        [
            'name' => 'personal-center',
            'code' => '&#xe70b;'
        ],

        [
            'name' => 'integral',
            'code' => '&#xe70c;'
        ],

        [
            'name' => 'operation',
            'code' => '&#xe70e;'
        ],

        [
            'name' => 'remind',
            'code' => '&#xe713;'
        ],

        [
            'name' => 'download',
            'code' => '&#xe714;'
        ],

        [
            'name' => 'map',
            'code' => '&#xe715;'
        ],

        [
            'name' => 'bad',
            'code' => '&#xe716;'
        ],

        [
            'name' => 'good',
            'code' => '&#xe717;'
        ],

        [
            'name' => 'skip',
            'code' => '&#xe718;'
        ],

        [
            'name' => 'play',
            'code' => '&#xe719;'
        ],

        [
            'name' => 'stop',
            'code' => '&#xe71a;'
        ],

        [
            'name' => 'compass',
            'code' => '&#xe71b;'
        ],

        [
            'name' => 'security',
            'code' => '&#xe71c;'
        ],

        [
            'name' => 'share',
            'code' => '&#xe71d;'
        ],

        [
            'name' => 'store',
            'code' => '&#xe722;'
        ],

        [
            'name' => 'manage-order',
            'code' => '&#xe723;'
        ],

        [
            'name' => 'rejected-order',
            'code' => '&#xe724;'
        ],

        [
            'name' => 'phone',
            'code' => '&#xe725;'
        ],

        [
            'name' => 'bussiness-man',
            'code' => '&#xe726;'
        ],

        [
            'name' => 'shoes',
            'code' => '&#xe728;'
        ],

        [
            'name' => 'Mobile-phone',
            'code' => '&#xe72a;'
        ],

        [
            'name' => 'email-filling',
            'code' => '&#xe72d;'
        ],

        [
            'name' => 'favorites-filling',
            'code' => '&#xe730;'
        ],

        [
            'name' => 'account-filling',
            'code' => '&#xe732;'
        ],

        [
            'name' => 'credit-level',
            'code' => '&#xe735;'
        ],

        [
            'name' => 'credit-level-filling',
            'code' => '&#xe736;'
        ],

        [
            'name' => 'exl',
            'code' => '&#xe73f;'
        ],

        [
            'name' => 'pdf',
            'code' => '&#xe740;'
        ],

        [
            'name' => 'zip',
            'code' => '&#xe741;'
        ],

        [
            'name' => 'sorting',
            'code' => '&#xe743;'
        ],

        [
            'name' => 'copy',
            'code' => '&#xe744;'
        ],

        [
            'name' => 'save',
            'code' => '&#xe747;'
        ],

        [
            'name' => 'inquiry-template',
            'code' => '&#xe749;'
        ],

        [
            'name' => 'template-default',
            'code' => '&#xe74a;'
        ],

        [
            'name' => 'libra',
            'code' => '&#xe74c;'
        ],

        [
            'name' => 'survey',
            'code' => '&#xe74e;'
        ],

        [
            'name' => 'ship',
            'code' => '&#xe74f;'
        ],

        [
            'name' => 'bussiness-card',
            'code' => '&#xe753;'
        ],

        [
            'name' => 'hot',
            'code' => '&#xe756;'
        ],

        [
            'name' => 'data',
            'code' => '&#xe757;'
        ],

        [
            'name' => 'trade',
            'code' => '&#xe758;'
        ],

        [
            'name' => 'onepage',
            'code' => '&#xe75a;'
        ],

        [
            'name' => 'signboard',
            'code' => '&#xe75c;'
        ],

        [
            'name' => 'shuffling-banner',
            'code' => '&#xe75e;'
        ],

        [
            'name' => 'component',
            'code' => '&#xe75f;'
        ],

        [
            'name' => 'component-filling',
            'code' => '&#xe760;'
        ],

        [
            'name' => 'color',
            'code' => '&#xe761;'
        ],

        [
            'name' => 'color-filling',
            'code' => '&#xe7cd;'
        ],

        [
            'name' => 'favorites',
            'code' => '&#xe7ce;'
        ],

        [
            'name' => 'pic-filling',
            'code' => '&#xe802;'
        ],

        [
            'name' => 'RFQ',
            'code' => '&#xe803;'
        ],

        [
            'name' => 'RFQ-filling',
            'code' => '&#xe804;'
        ],

        [
            'name' => 'original-image',
            'code' => '&#xe806;'
        ],

        [
            'name' => 'logistic',
            'code' => '&#xe811;'
        ],

        [
            'name' => 'Calculator',
            'code' => '&#xe812;'
        ],

        [
            'name' => 'video',
            'code' => '&#xe820;'
        ],

        [
            'name' => 'earth',
            'code' => '&#xe828;'
        ],

        [
            'name' => 'task-management',
            'code' => '&#xe829;'
        ],

        [
            'name' => 'trust',
            'code' => '&#xe82a;'
        ],

        [
            'name' => 'password',
            'code' => '&#xe82b;'
        ],

        [
            'name' => '3column',
            'code' => '&#xe839;'
        ],

        [
            'name' => 'apparel',
            'code' => '&#xe83a;'
        ],

        [
            'name' => 'bags',
            'code' => '&#xe83b;'
        ],

        [
            'name' => 'folder',
            'code' => '&#xe83c;'
        ],

        [
            'name' => '4column',
            'code' => '&#xe83d;'
        ],

        [
            'name' => 'code',
            'code' => '&#xe842;'
        ],

        [
            'name' => 'RFQ-filling',
            'code' => '&#xe843;'
        ],
    ],
];
