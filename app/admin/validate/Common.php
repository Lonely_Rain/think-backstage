<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Common extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
	    'status|状态' => ['require', 'between:0,1'],
        'sort|排序' => ['require', 'number'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'status' => ['status'],
        'sort' => ['sort']
    ];
}
