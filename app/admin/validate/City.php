<?php
declare (strict_types=1);

namespace app\admin\validate;

use think\Validate;

class City extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'name|名称' => ['require', 'length:1,80'],
        'code|编码' => ['require', 'number'],
        'province_id|省' => ['number'],
        'city_id|市' => ['number'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'create' => ['name', 'code', 'province_id', 'city_id'],

        'edit' => ['name', 'code', 'province_id', 'city_id'],
    ];
}
