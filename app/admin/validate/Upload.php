<?php
declare (strict_types=1);

namespace app\admin\validate;

use think\Validate;

class Upload extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'img|图片' => ['file', 'filesize:2097152', 'fileExt:jpg,png,gif', 'fileMime:image/png,image/jpeg,image/jpg,image/gif'],
        'video|视频' => ['file', 'filesize:20971520', 'fileExt:mp4,webm,ogg', 'fileMime:video/mp4,video/webm,image/jpg,video/ogg'],
        'file|文件' => ['file', 'filesize:20971520'],
        'block|文件块' => ['file', 'filesize:20971520'],

        'name|文件名称' => ['require'],
        'size|文件总大小' => ['require', 'number'],
        'totalSheet|文件总片数' => ['require', 'number'],
        'key|文件key' => ['require'],
        'md5|文件md5' => ['require'],
        'indexList|重置文件块' => ['require', 'array'],
        'index|文件块索引' => ['require', 'number'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'img.filesize' => '图片最大上传2M',
        'img.fileExt' => '图片后缀错误',
        'img.fileMime' => '图片类型错误',

        'video.filesize' => '视频最大上传20M',
        'video.fileExt' => '视频后缀错误',
        'video.fileMime' => '视频类型错误',

        'file.filesize' => '文件最大上传20M',
    ];

    protected $scene = [
        'image' => ['img'],

        'video' => ['video'],

        'file' => ['file'],

        // 分片上传
        'generateUploadFileKey' => ['name', 'size', 'totalSheet'],
        'upload' => ['key', 'md5', 'index', 'block'],
        'reset' => ['key', 'indexList'],
        'cancel' => ['key'],
    ];
}
