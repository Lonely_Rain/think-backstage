<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class AdminRole extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
	    'name|角色名称' => ['require', 'length:1,30'],
        'description|描述' => ['require', 'length:1,100'],
        'rules' => ['require'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'rules.require' => '请选择权限',
    ];

    protected $scene = [
        'create' => ['name', 'description', 'rules'],
        'edit' => ['name', 'description', 'rules'],
    ];
}
