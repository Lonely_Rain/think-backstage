<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Ad extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
	    'name|广告名称' => ['require', 'length:1,30'],
        'link|广告链接' => ['length:1,120'],
        'resource|广告图片' => ['require', 'length:1,200'],
        'cate_id|广告分类' => ['require', 'number'],
        'description|描述' => ['require', 'length:1,100'],
        'target|新窗口' => ['require', 'number', 'between:0,1'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'create' => ['name', 'link', 'resource', 'cate_id', 'description'],
        'edit' => ['name', 'link', 'resource', 'cate_id', 'description'],
        'target' => ['target'],
    ];
}
