<?php
declare (strict_types=1);

namespace app\admin\validate;

use think\Validate;

class Config extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'value|值' => ['require'],
        'description|描述' => ['require', 'length:1,100'],
        'value_type|值类型' => ['require', 'number', 'between:0,2'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [];
}
