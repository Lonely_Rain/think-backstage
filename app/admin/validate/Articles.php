<?php
declare (strict_types=1);

namespace app\admin\validate;

use think\Validate;

class Articles extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'title|文章标题' => ['require', 'length:1,30'],
        'classify_id|文章分类' => ['require', 'number'],
        'description|文章详情' => ['require'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'create' => ['title', 'classify_id', 'description'],
        'edit' => ['title', 'classify_id', 'description'],
    ];
}
