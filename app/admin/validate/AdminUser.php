<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class AdminUser extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
	    'username|名称' => ['require', 'length:3,15'],
        'email|邮箱' => ['require', 'email'],
        'mobile|手机号' => ['require', 'mobile'],
        'password|密码' => ['length:8,16'],
        'renew_password|重复密码' => ['length:8,16'],
        'roles' => ['require'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'roles.require' => '请选择角色',
    ];

    protected $scene = [
        'create'  =>  ['username','email', 'mobile', 'password', 'renew_password', 'roles'],
        'edit'  =>  ['username','email', 'mobile', 'password', 'renew_password'],
    ];
}
