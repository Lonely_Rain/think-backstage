<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Auth extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
	    'username|登录名' => ['require'],
        'password|密码' => ['require'],
        'code|验证码' => ['captcha'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'login' => ['username', 'password', 'code'],
    ];
}
