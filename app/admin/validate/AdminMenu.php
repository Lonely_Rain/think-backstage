<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class AdminMenu extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
	    'title|标题' => ['require', 'length:3,30'],
        'icon' => ['require', 'length:3, 30'],
        'uri' => ['require', 'length: 1, 30'],
        'roles' => ['require'],
        'parent_id|父级id' => ['require', 'number'],
        'description|描述' => ['require'],
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'roles.require' => '请选择角色',
    ];

    protected $scene = [
        'create' => ['title', 'icon', 'uri', 'roles', 'description', 'parent_id'],
        'edit' => ['title', 'icon', 'uri', 'roles', 'description', 'parent_id'],
    ];
}
