<?php

use think\migration\Seeder;

class AdminRoleRule extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'role_id' => 1,
                'rule_id' => 1,
            ],

            [
                'role_id' => 1,
                'rule_id' => 2,
            ],

            [
                'role_id' => 1,
                'rule_id' => 3,
            ],
        ];

        $this -> table('admin_role_rule') -> insert($data) -> saveData();
    }
}
