<?php

use think\migration\Seeder;

class AdminUser extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'username' => 'admin',
                'email' => 'admin@admin.com',
                'mobile' => '13023222222',
                'password' => 'baff53e67782191cd9ab9d6c25ecba82242fe983',
                'status' => 1
            ],

            [
                'id' => 2,
                'username' => 'test',
                'email' => 'test@admin.com',
                'mobile' => '15023322222',
                'password' => 'baff53e67782191cd9ab9d6c25ecba82242fe983',
                'status' => 1
            ]
        ];

        $this -> table('admin_users') -> insert($data) -> saveData();
    }
}
