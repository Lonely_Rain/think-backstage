<?php

use think\migration\Seeder;

class AdminRule extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => '所有权限',
                'method' => 'ANY',
                'routes' => '*',
                'status' => 1
            ],

            [
                'id' => 2,
                'name' => '登出',
                'method' => 'POST',
                'routes' => 'logout',
                'status' => 1
            ],

            [
                'id' => 3,
                'name' => '首页',
                'method' => 'GET',
                'routes' => implode("\n", ['index', 'welcome']),
                'status' => 1
            ]
        ];

        $this -> table('admin_rule') -> insert($data) -> saveData();
    }
}
