<?php

use think\migration\Seeder;

class AdminRoleMenu extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {

        $menus = range(1, 14);

        $data = array_reduce($menus, function ($result, $v){
            array_push($result, [
                'role_id' => 1,
                'menu_id' => $v
            ]);

            return $result;
        }, []);

        $this -> table('admin_role_menu') -> insert($data) -> saveData();
    }
}
