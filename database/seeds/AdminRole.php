<?php

use think\migration\Seeder;

class AdminRole extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => '超级管理员',
                'description' => '拥有所有权限',
                'status' => 1
            ],
        ];

        $this -> table('admin_role') -> insert($data) -> saveData();
    }
}
