<?php

use think\migration\Seeder;

class Config extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'key' => 'backstage_title',
                'value' => '后台管理系统',
                'name' => '后台标题',
                'description' => '后台标签页的标题',
                'value_type' => 0,
                'config_type' => 0,
            ],

            [
                'key' => 'backstage_logo',
                'value' => '后台管理系统-logo',
                'name' => '后台logo',
                'description' => '后台logo名称',
                'value_type' => 0,
                'config_type' => 0,
            ],

            [
                'key' => 'backstage_login_title',
                'value' => '后台管理员系统-登录',
                'name' => '后台登录名称',
                'description' => '后台登录名称',
                'value_type' => 0,
                'config_type' => 0,
            ],
        ];

        $this -> table('config') -> insert($data) -> saveData();
    }
}
