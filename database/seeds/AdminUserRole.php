<?php

use think\migration\Seeder;

class AdminUserRole extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'user_id' => 2,
                'role_id' => 1,
            ]
        ];

        $this -> table('admin_user_role') -> insert($data) -> saveData();
    }
}
