<?php

use think\migration\Seeder;

class AdminMenu extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'title' => '管理员管理',
                'description' => '管理员管理',
                'icon' => '&#xe726;',
                'uri' => '/',
                'parent_id' => 0,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 2,
                'title' => '管理员列表',
                'description' => '管理员列表',
                'icon' => '&#xe6a7;',
                'uri' => '/admin-user',
                'parent_id' => 1,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 3,
                'title' => '角色管理',
                'description' => '角色管理',
                'icon' => '&#xe6a7;',
                'uri' => '/admin-role',
                'parent_id' => 1,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 4,
                'title' => '菜单管理',
                'description' => '菜单管理',
                'icon' => '&#xe6a7;',
                'uri' => '/admin-menu',
                'parent_id' => 1,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 5,
                'title' => '权限管理',
                'description' => '权限管理',
                'icon' => '&#xe6a7;',
                'uri' => '/admin-rule',
                'parent_id' => 1,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 6,
                'title' => '程序配置',
                'description' => '程序配置信息',
                'icon' => '&#xe6ae;',
                'uri' => '/',
                'parent_id' => 0,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 7,
                'title' => '基本配置',
                'description' => '程序的基本配置信息',
                'icon' => '&#xe6a7;',
                'uri' => '/config-base',
                'parent_id' => 6,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 8,
                'title' => '网站配置',
                'description' => '网站端的一些配置',
                'icon' => '&#xe6a7;',
                'uri' => '/config-web',
                'parent_id' => 6,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 9,
                'title' => '广告管理',
                'description' => '广告管理',
                'icon' => '&#xe6a8;',
                'uri' => '/',
                'parent_id' => 0,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 10,
                'title' => '广告分类',
                'description' => '广告分类',
                'icon' => '&#xe6a7;',
                'uri' => '/ad-cate',
                'parent_id' => 9,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 11,
                'title' => '广告列表',
                'description' => '广告列表',
                'icon' => '&#xe6a7;',
                'uri' => '/ad-list',
                'parent_id' => 9,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 12,
                'title' => '文章管理',
                'description' => '文章管理',
                'icon' => '&#xe6b5;',
                'uri' => '/',
                'parent_id' => 0,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 13,
                'title' => '文章分类',
                'description' => '管理文章分类',
                'icon' => '&#xe6a7;',
                'uri' => '/article-cate',
                'parent_id' => 12,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 14,
                'title' => '文章列表',
                'description' => '管理文章列表',
                'icon' => '&#xe6a7;',
                'uri' => '/article',
                'parent_id' => 12,
                'sort' => 0,
                'status' => 1,
            ],

            [
                'id' => 15,
                'title' => '地区配置',
                'description' => '地区配置',
                'icon' => '&#xe6a7;',
                'uri' => '/city',
                'parent_id' => 6,
                'sort' => 0,
                'status' => 1,
            ],
        ];

        $this -> table('admin_menu') -> insert($data) -> saveData();
    }
}
