<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminActionLog extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this -> table('admin_action_log', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '操作日志表']);

        $table -> addColumn('user_id', 'integer', [
            'limit' => 11,
            'default' => 0,
            'comment' => '管理员id',
        ]) -> addColumn('path', 'string', [
            'limit' => 150,
            'default' => '',
            'comment' => '操作地址',
        ]) -> addColumn('method', 'string', [
            'limit' => 20,
            'default' => '',
            'comment' => '请求方式',
        ]) -> addColumn('ip', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => '客户端ip',
        ]) -> addColumn('input', 'text', [
            'null' => true,
            'default' => null,
            'comment' => '请求参数',
        ]) -> addTimestamps() -> create();
    }
}
