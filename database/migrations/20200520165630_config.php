<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Config extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this -> table('config', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '配置表']);

        $table -> addColumn('key', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => '键名',
        ]) -> addColumn('value', 'text', [
            'null' => true,
            'default' => null,
            'comment' => '键值',
        ]) -> addColumn('name', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => '名称',
        ]) -> addColumn('description', 'string', [
            'limit' => 150,
            'default' => '',
            'comment' => '描述',
        ]) -> addColumn('value_type', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '0 文本 1 图片 2 富文本',
        ]) -> addColumn('config_type', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '配置类型 0 基本配置 1 网站配置',
        ]) -> addTimestamps() -> addSoftDelete() -> addIndex(['key', 'delete_time'], [
            'unique' => true,
            'name' => 'key_unique'
        ]) -> create();
    }
}
