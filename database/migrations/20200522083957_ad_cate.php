<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdCate extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this -> table('ad_cate', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '广告分类表']);

        $table -> addColumn('name', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => '广告位置名称',
        ]) -> addColumn('width', 'string', [
            'limit' => 30,
            'default' => '',
            'comment' => '广告位置宽度',
        ]) -> addColumn('height', 'string', [
            'limit' => 30,
            'default' => '',
            'comment' => '广告位置高度',
        ]) -> addColumn('description', 'string', [
            'limit' => 150,
            'default' => '',
            'comment' => '广告位置描述',
        ]) -> addTimestamps() -> addSoftDelete() -> addIndex(['name', 'delete_time'], [
            'unique' => true,
            'name' => 'name_unique'
        ]) -> create();
    }
}
