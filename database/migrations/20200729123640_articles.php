<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Articles extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this -> table('articles', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '文章表']);

        $table -> addColumn('title', 'string', [
            'limit' => 100,
            'default' => '',
            'comment' => '标题',
        ]) -> addColumn('description', 'text', [
            'null' => true,
            'default' => null,
            'comment' => '内容',
        ]) -> addColumn('classify_id', 'integer', [
            'limit' => 11,
            'default' => 0,
            'comment' => '分类id',
        ]) -> addColumn('status', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '状态',
        ]) -> addColumn('sort', 'integer', [
            'limit' => 4,
            'default' => 0,
            'comment' => '排序',
        ]) -> addTimestamps() -> create();
    }
}
