<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Ad extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this -> table('ad', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '广告表']);

        $table -> addColumn('name', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => '广告名称',
        ]) -> addColumn('link', 'string', [
            'limit' => 150,
            'default' => '',
            'comment' => '链接地址',
        ]) -> addColumn('resource', 'string', [
            'limit' => 150,
            'default' => '',
            'comment' => '广告资源',
        ]) -> addColumn('cate_id', 'integer', [
            'limit' => 11,
            'default' => 0,
            'comment' => '广告位置id',
        ]) -> addColumn('sort', 'integer', [
            'limit' => 11,
            'default' => 0,
            'comment' => '广告排序',
        ]) -> addColumn('status', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '广告状态 0 禁用 1 启用',
        ]) -> addColumn('target', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '是否新窗口打开 0 否 1 是',
        ]) -> addColumn('description', 'string', [
            'limit' => 150,
            'default' => '',
            'comment' => '广告描述',
        ]) -> addTimestamps() -> addSoftDelete() -> addIndex(['name', 'cate_id', 'delete_time'], [
            'unique' => true,
            'name' => 'name_cate_unique'
        ]) -> create();
    }
}
