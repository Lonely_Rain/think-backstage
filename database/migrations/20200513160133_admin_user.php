<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminUser extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this -> table('admin_users', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '管理员表']);

        $table -> addColumn('username', 'string', [
            'limit' => 20,
            'default' => '',
            'comment' => '用户名称',
        ]) -> addColumn('email', 'string', [
            'limit' => 30,
            'default' => '',
            'comment' => '邮箱',
        ]) -> addColumn('mobile', 'string', [
            'limit' => 11,
            'default' => '',
            'comment' => '手机号',
        ]) -> addColumn('password', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => '密码',
        ]) -> addColumn('status', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '状态 0 未启用 1 启用',
        ]) -> addTimestamps() -> addSoftDelete() -> addIndex(['username', 'delete_time'], [
            'unique' => true,
            'name' => 'username_unique'
        ]) -> addIndex(['email', 'delete_time'], [
            'unique' => true,
            'name' => 'email_unique'
        ]) -> addIndex(['mobile', 'delete_time'], [
            'unique' => true,
            'name' => 'mobile_unique'
        ]) -> create();
    }
}
