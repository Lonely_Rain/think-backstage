<?php

use think\migration\Migrator;
use think\migration\db\Column;

class ArticleCate extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this -> table('article_cate', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '文章分类表']);

        $table -> addColumn('name', 'string', [
            'limit' => 100,
            'default' => '',
            'comment' => '名称',
        ]) -> addColumn('status', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '状态',
        ]) -> addColumn('sort', 'integer', [
            'limit' => 4,
            'default' => 0,
            'comment' => '排序',
        ]) -> addTimestamps() -> create();
    }
}
