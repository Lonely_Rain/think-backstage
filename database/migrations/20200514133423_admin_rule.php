<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminRule extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this -> table('admin_rule', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '管理员权限表']);

        $table -> addColumn('name', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => '名称',
        ]) -> addColumn('method', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => '请求方式',
        ]) -> addColumn('routes', 'text', [
            'limit' => 0,
            'default' => null,
            'comment' => '路由',
        ]) -> addColumn('status', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '状态 0 未启用 1 启用',
        ]) -> addTimestamps() -> addSoftDelete() -> addIndex(['name', 'method', 'delete_time'], [
            'unique' => true,
            'name' => 'name_method_unique'
        ]) -> create();
    }
}
