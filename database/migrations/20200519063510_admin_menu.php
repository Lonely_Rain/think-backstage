<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminMenu extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this -> table('admin_menu', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '菜单表']);

        $table -> addColumn('title', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => '菜单名称',
        ]) -> addColumn('description', 'string', [
            'limit' => 150,
            'default' => '',
            'comment' => '菜单描述',
        ]) -> addColumn('icon', 'string', [
            'limit' => 50,
            'default' => '',
            'comment' => 'icon图标名称',
        ]) -> addColumn('uri', 'string', [
            'limit' => 100,
            'default' => '',
            'comment' => 'uri地址',
        ]) -> addColumn('parent_id', 'integer', [
            'limit' => 11,
            'default' => 0,
            'comment' => '父级id',
        ]) -> addColumn('sort', 'integer', [
            'limit' => 11,
            'default' => 0,
            'comment' => '排序',
        ]) -> addColumn('status', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '状态 0 未启用 1 启用',
        ]) -> addTimestamps() -> addSoftDelete() -> addIndex(['title', 'parent_id', 'delete_time'], [
            'unique' => true,
            'name' => 'title_parentId_unique'
        ]) -> create();
    }
}
