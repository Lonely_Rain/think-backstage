<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Area extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('area', ['collation' => 'utf8mb4_unicode_ci', 'comment' => '地区表']);

        $table->addColumn('name', 'string', [
            'limit' => 100,
            'default' => '',
            'comment' => '名称',
        ])->addColumn('code', 'integer', [
            'limit' => 1,
            'default' => 0,
            'comment' => '编码',
        ])->addColumn('pid', 'json', [
            'null' => true,
            'default' => null,
            'comment' => '{"province_id": "省级id", "city_id": "市级id"}',
        ])->addColumn('sort', 'integer', [
            'limit' => 4,
            'default' => 0,
            'comment' => '排序',
        ])->addColumn('status', 'integer', [
            'limit' => 4,
            'default' => 0,
            'comment' => '状态',
        ])->addTimestamps()->addIndex(['code'], [
            'unique' => true,
            'name' => 'code_unique'
        ])->create();
    }
}
