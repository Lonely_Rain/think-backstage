import {rain} from '../../app.js';

/**
 * 更新状态
 *
 * @param id
 * @param data
 * @returns {{}}
 */
export function status(id, data) {
    return load(rain.request(`city/${id}/status`, 'put', data));
}

/**
 * 更新排序
 *
 * @param id
 * @param data
 * @returns {{}}
 */
export function sort(id, data) {
    return load(rain.request(`city/${id}/sort`, 'put', data));
}