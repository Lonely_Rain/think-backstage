import { rain } from '../../app.js';
import { getDomain } from "../../request.js";

/**
 *
 * 编辑资源页面
 *
 * @param uri
 * @param id
 */
export function edit(uri, id){
    xadmin.open('编辑配置', getDomain() + `${uri}/${id}/edit`);
}

/**
 * 更新数据
 *
 * @param id
 * @param data
 * @returns {{}}
 */
export function update(id, data){
    return load(rain.request(`config-base/${id}`, 'put', data));
}
