import { rain } from '../../app.js';
import { getDomain } from "../../request.js";

/**
 * 创建资源页面
 */
export function create(){
    xadmin.open('添加广告分类', getDomain() + 'ad-cate/create');
}

/**
 * 编辑资源页面
 *
 * @param id
 */
export function edit(id){
    xadmin.open('编辑广告分类', getDomain() + `ad-cate/${id}/edit`);
}

/**
 * 创建数据
 *
 * @param data
 * @returns {{}}
 */
export function save(data){
    return load(rain.request(`ad-cate`, 'post', data));
}

/**
 * 更新数据
 *
 * @param id
 * @param data
 * @returns {{}}
 */
export function update(id, data){
    return load(rain.request(`ad-cate/${id}`, 'put', data));
}

/**
 * 删除数据
 *
 * @param id
 * @returns {{}}
 */
export function del(id){
    return load(rain.request(`ad-cate/${id}`, 'delete'));
}
