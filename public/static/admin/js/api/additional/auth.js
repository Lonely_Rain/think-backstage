import { rain } from '../../app.js';

/**
 * 登录
 *
 * @param data
 * @returns {{}}
 */
export function login(data){
    return load(rain.request('login', 'post', data));
}

/**
 * 退出登录
 *
 * @returns {{}}
 */
export function logout(){
    return load(rain.request('logout', 'post'), false);
}

/**
 * 替换 token
 *
 * @param data
 * @returns {*}
 */
export function exchangeToken(data){
    return rain.request('exchangeToken', 'post', data);
}

