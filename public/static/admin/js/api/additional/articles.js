import { rain } from '../../app.js';
import { getDomain } from "../../request.js";

/**
 * 创建资源页面
 */
export function create(){
    xadmin.open('添加文章', getDomain() + 'article/create');
}

/**
 * 编辑资源页面
 *
 * @param id
 */
export function edit(id){
    xadmin.open('编辑文章', getDomain() + `article/${id}/edit`);
}

/**
 * 更新状态
 *
 * @param id
 * @param data
 * @returns {{}}
 */
export function status(id, data){
    return load(rain.request(`article/${id}/status`, 'put', data));
}

/**
 * 更新排序
 *
 * @param id
 * @param data
 * @returns {{}}
 */
export function sort(id, data){
    return load(rain.request(`article/${id}/sort`, 'put', data));
}

/**
 * 创建数据
 *
 * @param data
 * @returns {{}}
 */
export function save(data){
    return load(rain.request(`article`, 'post', data));
}

/**
 * 更新数据
 *
 * @param id
 * @param data
 * @returns {{}}
 */
export function update(id, data){
    return load(rain.request(`article/${id}`, 'put', data));
}

/**
 * 删除数据
 *
 * @param id
 * @returns {{}}
 */
export function del(id){
    return load(rain.request(`article/${id}`, 'delete'));
}
