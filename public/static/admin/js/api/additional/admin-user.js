import { rain } from '../../app.js';
import { getDomain } from "../../request.js";


/**
 * 创建资源页面
 */
export function create(){
    xadmin.open('添加管理员', getDomain() + 'admin-user/create');
}

/**
 * 编辑资源页面
 *
 * @param id
 */
export function edit(id){
    xadmin.open('编辑管理员', getDomain() + `admin-user/${id}/edit`);
}

/**
 * 更新状态
 *
 * @param id
 * @param data
 * @returns {{}}
 */
export function status(id, data){
    return load(rain.request(`admin-user/${id}/status`, 'put', data));
}

/**
 * 创建数据
 *
 * @param data
 * @returns {{}}
 */
export function save(data){
    return load(rain.request(`admin-user`, 'post', data));
}

/**
 * 更新数据
 *
 * @param id
 * @param data
 * @returns {{}}
 */
export function update(id, data){
    return load(rain.request(`admin-user/${id}`, 'put', data));
}

/**
 * 删除数据
 *
 * @param id
 * @returns {{}}
 */
export function del(id){
    return load(rain.request(`admin-user/${id}`, 'delete'));
}
