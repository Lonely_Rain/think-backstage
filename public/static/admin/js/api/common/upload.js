import {rain} from '../../app.js';

/**
 * 上传图片
 *
 * @param data
 * @param config
 * @returns {{}}
 */
export function img(data, config = {}) {
    return load(rain.request('upload-img', 'post', data, config));
}

/**
 * 上传视频
 *
 * @param data
 * @param config
 * @returns {{}}
 */
export function video(data, config = {}) {
    return load(rain.request('upload-video', 'post', data, config));
}

/**
 * 分片上传，获取请求 key
 *
 * @param data
 * @returns {{}}
 */
export function getUploadKey(data) {
    return rain.request('multipart-upload/key', 'get', data);
}

/**
 * 分片上传，重置文件
 *
 * @param data
 * @returns {{}}
 */
export function reset(data) {
    return rain.request('multipart-upload/reset', 'post', data);
}

/**
 * 分片上传，取消上传
 *
 * @param data
 * @returns {{}}
 */
export function cancel(data) {
    return rain.request('multipart-upload/cancel', 'post', data);
}

/**
 * 分片上传
 *
 * @param data
 * @param config
 * @returns {{}}
 */
export function sheetUpload(data, config) {
    return rain.request('multipart-upload/upload', 'post', data, config);
}
