import {rain} from '../../app.js';

/**
 * 获取区域列表
 *
 * @param id
 * @param type
 * @returns {{}}
 */
export function getCityList(id, type) {
    return load(rain.request(`city/${id}/get-city-list/${type}`, 'get'), false);
}
