// 密钥
export const KEY = "base64:dmRZPSu&NHTYnXwEXY2!FwNiZhr9lxCQ";

/**
 * 请求公共属性
 *
 * @type {{url: {dev: string, produce: string}, header: {"Content-type": string}}}
 */
let base = {
    app: 'admin',

    url: {
        dev: 'http://www.think-back.com',
        produce: '',
    },

    headers: {
        'Content-type': 'application/x-www-form-urlencoded',
    },
};

/**
 * 获取应用域名
 *
 * @returns {string}
 */
export let getDomain = () => {
    return base.url.dev + '/' + base.app + '/';
};

/**
 * 获取文件存储根目录
 *
 * @returns {string}
 */
export let getFileDomain = () => {
    return base.url.dev + "/storage/";
};

/**
 * 获取 csrf-token
 *
 * @returns {jQuery}
 */
export let getCsrfToken = () => {
    return $('meta[name="csrf-token"]', top.window.document).attr('content');
};

/**
 * 请求方式
 *
 * @param url
 * @param method
 * @param data
 * @param config
 * @returns {Promise<any>}
 */
export let request = (url, method, data = {}, config = {}) => {
    // 重新设置 token
    base.headers["X-CSRF-TOKEN"] = getCsrfToken();

    // 请求
    return new Promise((resolve, reject) => {
        $.ajax({
            headers: base.headers,

            url: getDomain() + url,
            type: method,
            data,
            dataType: 'json',

            success(res) {
                resolve(res)
            },
            error(res) {
                reject(res)
            },

            // 额外配置
            ...config,
        });
    });
};
