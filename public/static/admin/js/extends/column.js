let column = {
    prefix: '',

    child: (menus) => {
        let html = '';

        for(let i in menus){
            html += '<li>';

            if(menus[i].children.length == 0) {
                // 导航栏目
                html += '<li>\n' +
                    '                        <a onclick="xadmin.add_tab(\''+ menus[i].title +'\',\''+ column.prefix + menus[i].uri +'\')">\n' +
                    '                            <i class="iconfont">' + menus[i].icon + '</i>\n' +
                    '                            <cite>'+ menus[i].title +'</cite></a>\n' +
                    '                    </li>';
            } else {
                // 导航菜单
                html += '<a href="javascript:;">\n' +
                    '                    <i class="iconfont left-nav-li" lay-tips="'+ menus[i].title +'">'+ menus[i].icon +'</i>\n' +
                    '                    <cite>'+menus[i].title+'</cite>\n' +
                    '                    <i class="iconfont nav_right">&#xe697;</i></a>';

                html += '<ul class="sub-menu">';

                // 子导航栏
                html += column.child(menus[i].children);

                html += '</ul>';
            }

            html += '</li>';
        }

        return html;
    },
};

export default column;
