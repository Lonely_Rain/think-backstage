import {getCsrfToken} from "../request.js";

/**
 * 初始化配置
 *
 * @param config
 * @param initConfig
 */
export function uploadInit(config, initConfig) {
    // 操作按钮隐藏
    $(config.config.bindAction).hide();
    $(config.config.resetAction).hide();

    // 是否传入自定义配置
    if (typeof initConfig.resource != 'undefined' && Object.keys(initConfig.resource).length != 0) {
        $(config.config.resetAction).show();

        // 默认重置资源
        $(config.config.resetAction).one('click', () => {
            // 操作按钮隐藏
            $(config.config.bindAction).hide();
            $(config.config.resetAction).hide();

            // 触发自定义方法
            config.customize.resetResource();
        })
    }

    // 自定义回调
    config.customize.init();
}

/**
 * 处理按钮显示和隐藏
 *
 * @param object        当前上传组件对象
 * @param isShow        显示 或 隐藏
 */
export function handlerBtn(object, isShow) {
    // 默认隐藏
    object.bindAction.hide();
    $(object.resetAction).hide();

    // 是否显示
    if (isShow) {
        object.bindAction.show();
        $(object.resetAction).show();
    }

    object.bindAction.text('开始上传');
}

/**
 * 清除上传文件列表
 *
 * @param files
 */
export function clearFiles(files) {
    for (let i in files) delete files[i];
}

/**
 * 设置headers头
 *
 *
 * @returns {{"X-CSRF-TOKEN": jQuery}}
 */
export function setHeaders() {
    return {
        "X-CSRF-TOKEN": getCsrfToken()
    };
}
