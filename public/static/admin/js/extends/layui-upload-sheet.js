import {sheetUpload} from "./sheet-upload.js";

/**
 * 分片上传
 *
 * @param config
 * @param upload
 */
export function sheet(config, upload) {
    // 取消事件绑定
    $(config.config.bindAction).unbind();

    // 自定义点击事件
    $(config.config.bindAction).click(() => {
        // 上传多个文件，同步上传
        (async () => {
            // 开始加载
            upload.before();

            // 失败结果 type = 0 文件 1 分片
            let fail = [];

            for (let i in upload.customize.files) {
                let result = await fileUploadHandler(i, upload.customize.files[i]);

                // 上传失败，则添加失败队列
                if (200 !== result.code) fail.push(result.data);

                // 触发回调
                upload.done(result, i);
            }

            // 判断是否文件有上传失败
            if (fail.length !== 0) resetUpload(config, upload, fail);
        })();
    });
}

/**
 * 重新上传
 *
 * @param config
 * @param upload
 * @param fail
 */
function resetUpload(config, upload, fail) {
    // 取消事件绑定
    $(config.config.bindAction).unbind();

    // 自定义点击事件
    $(config.config.bindAction).click(() => {
        // 上传多个文件，同步上传
        (async () => {
            // 失败结果 type = 0 文件 1 分片
            let failResult = [];

            // 开始加载
            upload.before();

            for (let i in fail) {
                let result;

                if (0 === fail[i].type) {
                    // 失败文件上传
                    result = await fileUploadHandler(i, fail[i].file);

                    // 上传失败，则添加失败队列
                    if (200 !== result.code) failResult.push(result.data);
                } else {
                    // 分片重置上传
                    result = await resetUploadHandler(fail[i].index, fail[i].key, fail[i].sheet, fail[i].file);

                    // 上传失败，则添加失败队列
                    if (200 !== result.code) failResult.push(result.data);
                }

                // 触发回调
                upload.done(result, i);
            }

            // 判断是否文件有上传失败
            if (failResult.length !== 0) resetUpload(config, upload, failResult);
        })();
    });
}

/**
 * 处理文件上传
 *
 * @param file
 * @param index
 * @returns {Promise<{code: number, data: {file: *, type: number}}|{code: number, data: {file: *, sheet: *[], type: number, key: *}}|{code: number}>}
 */
export async function fileUploadHandler(index, file) {
    // 分片上传
    let result = await sheetUpload.boot(file);

    // 验证是否上传错误
    if (200 !== result.code) return {code: 500, data: {type: 0, index, file}};

    // 验证分片是否有上传失败
    if (result.data.file.fail.length !== 0) return {
        code: 500,
        data: {
            index,
            type: 1,
            file,
            sheet: result.data.file.fail,
            key: result.data.key
        }
    };

    // 获取input
    let input = '';
    for (let item of result.data.file.file) {
        input = item.result.data.input;

        if (item.result.data.isMerge) break;
    }

    return {code: 200, msg: '请求成功', data: {input}};
}

/**
 * 重新上传处理
 *
 * @param key
 * @param sheet
 * @param file
 * @param index
 * @returns {Promise<{code: number, data: {file: *, sheet: *, type: number, key: *}}|{code: number, data: {file: *, sheet: *[], type: number, key: *}}|{msg: string, code: number, data: {input: string}}>}
 */
async function resetUploadHandler(key, sheet, file, index) {
    // 分片重置上传
    let result = await sheetUpload.resetUpload(key, sheet);

    // 失败则添加公共配置
    let data = {
        type: 1,
        index,
        file,
        key
    };

    // 验证是否上传错误
    if (result.data.file.fail.length !== 0) return {code: 500, data: {sheet: sheet, ...data}};

    // 验证分片重新上传是否失败
    if (result.code !== 200) return {code: 500, data: {sheet: result.data.file.fail, ...data}};

    // 获取input
    let input = '';
    for (let item of result.data.file.file) {
        input = item.result.data.input;

        if (item.result.data.isMerge) break;
    }

    return {code: 200, msg: '请求成功', data: {input}};
}