let children = {
    el: '',         // 某个字段需添加的标识
    cateIds: [],
    pid: '',        // 父级 字段 名

    init(data) {
        $("th").css({'color': 'black'});

        for (var i in  data) {
            $("tr[data-index='" + i + "']").attr('cate-id', data[i].id);
            $("tr[data-index='" + i + "']").attr('pid', data[i][this.pid]);
            $("tr[data-index='" + i + "']").attr('data-name', data[i][this.el]);

            // 添加点击事件
            $("tr[data-index='" + i + "']").click((e) => {
                this.showTree(e.currentTarget);
            });

            $("tr[data-index='" + i + "']").attr('status', "true");
            $("tr[data-index='" + i + "']").attr('level', data[i].level - 1 < 0 ? 0 : data[i].level - 1);

            // 是否有子级
            if (data[i][this.pid] == 0) {

                let child = false;
                [...data].forEach(item => {
                    if(item[this.pid] == data[i].id) {
                        child = true;

                        return false;
                    }
                });

                if(child) $("tr[data-index='" + i + "']").find("td[data-field='" + this.el + "']")
                    .children().html('<i class="layui-icon x-show">&#xe623;</i>' + data[i][this.el]);
            }

        }
        $("tbody tr[pid!='0']").hide();
    },

    /**
     * 展示分类列表
     *
     * @param e
     */
    showTree(e) {
        let that = this;

        if ($(e).attr('status') == 'true') {
            $(e).find(".x-show").html('&#xe625;')
            $(e).attr('status', 'false');
            let cateId = $(e).attr('cate-id');
            let $arr = [];
            $("tbody tr[pid=" + cateId + "]").each(function (index, el) {
                let id = $(el).attr('cate-id');

                if ($arr[0] == id) return false;

                $arr.push(id);
                var child_cateId = $("tbody tr[pid=" + id + "]").attr('cate-id');

                var level = $("tbody tr[cate-id=" + id + "]").attr('level');
                var space = that.printSpace(level);

                if (typeof(child_cateId) != "undefined") {

                    $("tbody tr[cate-id=" + id + "]").find("td[data-field='" + that.el + "']").children()
                        .html(space + '<i class="layui-icon x-show">&#xe623;</i>' + $("tbody tr[cate-id=" + id + "]").attr('data-name'));
                } else {
                    $("tbody tr[cate-id=" + id + "]").find("td[data-field='" + that.el + "']").children()
                        .html(space + '<i class="layui-icon x-show"> |--</i>' + $("tbody tr[cate-id=" + id + "]").attr('data-name'));

                    $("tbody tr[cate-id=" + id + "]").unbind('click');
                }
            });

            $("tbody tr[pid=" + cateId + "]").show();

        } else {
            that.cateIds = [];
            $(e).find(".x-show").html('&#xe623;')
            $(e).attr('status', 'true');

            let cateId = $(e).attr('cate-id');
            that.getCateId(cateId);
            for (var i in that.cateIds) {
                $("tbody tr[cate-id=" + that.cateIds[i] + "]").hide().find('.x-show').html('&#xe623;').attr('status', 'true');
            }
        }
    },

    /**
     * 获取分类id
     *
     * @param cateId
     */
    getCateId(cateId) {
        let that = this;

        $("tbody tr[pid=" + cateId + "]").each(function (index, el) {
            let id = $(el).attr('cate-id');

            if (that.cateIds[0] == id) {
                return false;
            }

            that.cateIds.push(id);
            that.getCateId(id);
        });
    },

    /**
     * 根据级别打印空格
     *
     * @param level
     * @returns {string}
     */
    printSpace(level) {
        var str = "&nbsp;";
        if (level > 0) {
            for (var i = 0; i < level * 6; i++) {
                str = str + "&nbsp;";
            }

        }
        return str;
    }
};

export default children;
