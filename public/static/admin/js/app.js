import {request, KEY} from './request.js';

// 额外属性
export const rain = {
    request,

    // 基本配置
    config: {
        // 动态表格的配置
        table: {
            url: window.location.href,

            elem: '#table',
            page: true, //开启分页
            limit: 10,
            height: 'full-160',

            // 默认返回格式处理
            parseData(res) {
                let response = {
                    "code": 200 != res.code ? res.code : 0, //解析接口状态
                    "msg": res.msg, //解析提示文本
                };

                // 分页 和 无分页格式
                if (this.page) {
                    response.count = res.data.list.total; //解析数据长度
                    response.data = res.data.list.data; //解析数据列表
                } else {
                    response.count = res.data.list.length; //解析数据长度
                    response.data = res.data.list; //解析数据列表
                }

                return response;
            },
        },

        // xm-select配置
        xmSelect: {
            // 默认主题
            theme: {
                color: '#009688',
            },
        },

        // 错误码：刷新当前页面
        errors: [602, 603],

        key: KEY,
    },

    // 操作表格
    tableAction: {
        init: {
            form: {},
            table: {}
        },

        /**
         * 筛选
         *
         * @param tableId   表格id
         * @param page      是否分页
         */
        search(tableId, page = true) {
            this.init.form.on('submit(search)', (data) => {
                this.init.table.reload(tableId, {
                    page: page ? {
                        curr: 1 //重新从第 1 页开始
                    } : page
                    , where: {
                        key: data.field
                    }
                });

                return false;
            });
        },

        // 行内操作按钮
        tool: {
            /**
             * 行内删除操作
             *
             * @param layer 弹出组件
             * @param del   删除事件
             * @param data  行对象，删除数据
             */
            del(layer, del, data) {
                layer.confirm('确认要删除吗？', function (index) {
                    del(data.data.id).then(res => {
                        dialog(res.msg, () => {
                            if (200 != res.code) return;

                            data.obj.del();
                            layer.close(index);
                        })
                    });
                });
            }
        }
    }
};

// 禁止后退键  作用于IE、Chrome
document.onkeypress = forbidBackSpace;
// 禁止后退键  作用于IE、Chrome
document.onkeydown = forbidBackSpace;

// 加载模块
layui.config({
    base: '/static/admin/lib/',
}).extend({
    notice: 'notice/js/notice'
}).use(['notice'], () => {
    // 初始化配置
    layui.notice.options = {
        closeButton: true,//显示关闭按钮
        debug: false,//启用debug
        positionClass: "toast-top-right",//弹出的位置,
        showDuration: "300",//显示的时间
        hideDuration: "1000",//消失的时间
        timeOut: "2000",//停留的时间
        extendedTimeOut: "1000",//控制时间
        showEasing: "swing",//显示时的动画缓冲方式
        hideEasing: "linear",//消失时的动画缓冲方式
        iconClass: 'toast-info', // 自定义图标，有内置，如不需要则传空 支持layui内置图标/自定义iconfont类名
        onclick: null, // 点击关闭回调
    };
});




