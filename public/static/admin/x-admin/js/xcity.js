﻿let provinceList = [];

/**
 * 城市切换
 *
 * @param form      表单
 * @param value     默认值 { pValue: "省ID", cValue: "市ID"，aValue: "区ID" }
 * @param filter    监听 select的lay-filter 值 { province: "省",  city: "市" }
 * @returns {Promise<$>}
 */
$.fn.xcity = async function (form, value = {}, filter) {
    this.p = $(this).find('select[data-name=province]');

    this.c = $(this).find('select[data-name=city]');

    this.a = $(this).find('select[data-name=area]');

    this.cityList = [];

    this.reaList = [];

    this.showP = async function (provinceList) {
        this.p.html("<option value=''>请选择省</option>");

        is_pName = false;

        for (var i in provinceList) {
            if (parseInt(value.pValue) === provinceList[i].id) {
                is_pName = true;

                this.cityList = await getCityList(provinceList[i].id, 1);
                if (typeof this.cityList === 'undefined') return;

                this.p.append("<option selected  value='" + provinceList[i].id + "'>" + provinceList[i].name + "</option>")
            } else {
                this.p.append("<option value='" + provinceList[i].id + "'>" + provinceList[i].name + "</option>")
            }
        }

        if (!is_pName) this.cityList = [];
    }

    this.showC = async function (cityList) {
        this.c.html("<option value=''>请选择市</option>");

        is_cName = false;
        for (var i in cityList) {
            if (parseInt(value.cValue) === cityList[i].id) {
                is_cName = true;

                this.areaList = await getCityList(cityList[i].id, 2);
                if (typeof this.areaList === 'undefined') return;

                this.c.append("<option selected value='" + cityList[i].id + "'>" + cityList[i].name + "</option>")
            } else {
                this.c.append("<option value='" + cityList[i].id + "'>" + cityList[i].name + "</option>")
            }
        }

        if (!is_cName) this.areaList = [];
    }

    this.showA = function (areaList) {
        this.a.html("<option value=''>请选择区</option>");
        for (var i in areaList) {
            if (parseInt(value.aValue) === areaList[i].id) {
                this.a.append("<option selected value='" + areaList[i].id + "'>" + areaList[i].name + "</option>")
            } else {
                this.a.append("<option value='" + areaList[i].id + "'>" + areaList[i].name + "</option>")
            }
        }
    }

    // 获取省列表
    provinceList = provinceList.length === 0 ? await getCityList(0, 0) : provinceList;
    if (typeof provinceList === 'undefined') return;

    if (this.p.length !== 0) await this.showP(provinceList);
    if (this.c.length !== 0) await this.showC(this.cityList);
    if (this.a.length !== 0) this.showA(this.areaList);

    form.render('select');

    if (this.c.length !== 0) {
        form.on(`select(${filter.province})`, function (data) {
            $(data.elem).parents(".x-city").xcity(form, {pValue: data.value}, filter);
        });
    }

    if (this.a.length !== 0) {
        form.on(`select(${filter.city})`, function (data) {
            var pValue = $(data.elem).parents(".x-city").find('select[data-name=province]').val();

            $(data.elem).parents(".x-city").xcity(form, {pValue, cValue: data.value}, filter);
        });
    }

    return this;
}

/**
 * 获取地区列表
 *
 * @param id
 * @param type
 * @returns {Promise<any>}
 */
async function getCityList(id, type) {
    let area = await import('../../js/api/common/city.js').then(res => {
        return res.getCityList;
    })

    let result = await area(id, type);
    if (200 !== result.code) return dialog(result.msg);

    return result.data.list;
}
