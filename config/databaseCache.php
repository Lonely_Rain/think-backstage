<?php
/**
 * Created by PhpStorm.
 * User: Rain
 * Date: 2020/5/15
 * Time: 22:01
 */

return [
    'config' => [
        // 是否开启缓存
        'cache' => true,

        // 缓存时间
        'cache_time' => 60,
    ],

    // 模型 缓存
    'model' => [
        // 配置缓存
        \app\model\Config::class => [
            // 是否开启缓存
            'cache' => true,

            // 缓存时间
            'cache_time' => 80,

            // 缓存标签
            'cache_tag' => 'global_config',
        ],

        // 配置地区
        \app\model\Area::class => [
            // 是否开启缓存
            'cache' => true,

            // 缓存时间
            'cache_time' => 80,

            // 缓存标签
            'cache_tag' => 'global_area',
        ],
    ],
];
