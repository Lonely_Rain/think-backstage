<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'socket_client' => \app\command\SocketClient::class,

        'socket_server' => \app\command\SocketServer::class
    ],
];
