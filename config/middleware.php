<?php
// 中间件配置
return [
    // 别名或分组
    'alias' => [
        // 检查签名
        'CheckSign' => \app\middleware\CheckSign::class,

        // 检查权限
        'CheckAuth' => \app\middleware\CheckAuth::class,

        // 导栏管理
        'Nav' => \app\middleware\Nav::class,

        // 默认页面变量
        'Backstage' => \app\middleware\Backstage::class,

        // 记录操作日志
        'ActionLog' => \app\middleware\ActionLog::class,

        // 接口锁
        'ApiLock' => \app\middleware\ApiLock::class,
    ],
    // 优先级设置，此数组中的中间件会按照数组中的顺序优先执行
    'priority' => [],
];
