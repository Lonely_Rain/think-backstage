# 组件使用文档

#### 1、上传组件

```html
    <!-- HTML结构 -->
        
    <!-- 图片组件对应样式 -->
    <div class="layui-form-item">
        <label class="layui-form-label"><span class="x-red">*</span>上传图片</label>

        <div class="layui-input-block rain-resource">
            <button type="button" class="layui-btn layui-btn-normal" id="select-img">选择文件</button>
            <button type="button" class="layui-btn" id="upload-img">开始上传</button>
            <button type="button" class="layui-btn" id="reset-img">重置图片</button>

            <div class="layui-upload-list">
                <input type="hidden" name="resource">
                <div class="upload__img">
                    <img src="">
                </div>
            </div>
        </div>
    </div>
    
    <!-- 视频组件对应样式 -->
    <div class="layui-form-item">
        <label class="layui-form-label"><span class="x-red">*</span>上传视频</label>

        <div class="layui-input-block rain-resource">
            <button type="button" class="layui-btn layui-btn-normal" id="select-video">选择视频</button>
            <button type="button" class="layui-btn" id="upload-video">开始上传</button>
            <button type="button" class="layui-btn" id="reset-video">重置视频</button>

            <div class="layui-upload-list">
                <input type="hidden" name="resource">
                <div class="upload__video">
                    <video controls src=""></video>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 文件组件对应样式 -->
    <div class="layui-form-item">
        <label class="layui-form-label"><span class="x-red">*</span>上传文件</label>

        <div class="layui-input-block rain-resource">
            <button type="button" class="layui-btn layui-btn-normal" id="select-file">选择文件</button>
            <button type="button" class="layui-btn" id="upload-file">开始上传</button>
            <button type="button" class="layui-btn" id="reset-file">重置文件</button>

            <div class="layui-upload-list">
                <input type="hidden" name="resource">
                <div class="upload__file">
                    <span></span>
                </div>
            </div>
        </div>
    </div>
```

```js

    // js部分
    import { uploadApi } from "/static/admin/js/extends/layui-upload.js";

    // 赋值上传组件
    let upload = layui.upload;
    uploadApi.upload = upload;
    
    // 图片上传
    uploadApi.image({
        elem: '#select-img',
        bindAction: '#upload-img',
        resetAction: "#reset-img",
    }, {
        resource: {
            url: '',
            input: '',
        }
    });

    // 视频上传
    uploadApi.video({
        elem: '#select-video',
        bindAction: '#upload-video',
        resetAction: "#reset-video",
    }, {
        resource: {
            url: '',
            input: '',
        }
    });

    // 文件上传
    uploadApi.file({
        elem: '#select-file',
        bindAction: '#upload-file',
        resetAction: "#reset-file",
    }, {
        resource: {
            url: '',
            input: '',
        }
    });
    
```

#### 2、上传多张组件

```html
    <!-- HTML结构 -->
   
    <!-- 多张图片的HTML结构 -->
    <div class="layui-form-item">
        <label class="layui-form-label"><span class="x-red">*</span>多张图片</label>

        <div class="layui-input-block rain-resource">
            <button type="button" class="layui-btn layui-btn-normal" id="select-files-img">选择文件</button>
            <button type="button" class="layui-btn" id="upload-files">多图上传</button>
            <button type="button" class="layui-btn" id="reset-files-img">重置图片</button>

            <blockquote class="layui-elem-quote layui-quote-nm rain-resource__resource-box" style="margin-top: 10px;">
                预览图：
                <div class="layui-upload-list"></div>
            </blockquote>
        </div>
    </div>
        
    <!-- 图片组件对应样式 -->
    <div class="upload__item">
        <!-- 资源模块 -->
        <div class="upload__img">
            <img src="https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3616335013,3893781929&fm=26&gp=0.jpg">
        </div>

        <!-- 操作按钮 -->
        <div class="upload__resource__action_btn upload__resource__action_btn--left"><i class="layui-icon">&#xe603;</i></div>
        <div class="upload__resource__action_btn upload__resource__action_btn--right"><i class="layui-icon">&#xe602;</i></div>
        <div class="upload__resource__action_btn upload__resource__action_btn--close"><i class="layui-icon">&#x1006;</i></div>

        <!-- 上传状态 -->
        <div class="upload__resource__notify upload__resource__notify--none"><span></span></div>
    </div>
    
    <!-- 视频组件对应样式 -->
    <div class="upload__item">
        <!-- 资源模块 -->
        <div class="upload__video">
            <video controls src="http://www.jda.com/storage/temp/videos/2020-09-10/44/b6c45804983c0f9c8797ff75b8126663eb3efb.mp4"></video>
        </div>

        <!-- 操作按钮 -->
        <div class="upload__resource__action_btn upload__resource__action_btn--left"><i class="layui-icon">&#xe603;</i></div>
        <div class="upload__resource__action_btn upload__resource__action_btn--right"><i class="layui-icon">&#xe602;</i></div>
        <div class="upload__resource__action_btn upload__resource__action_btn--close"><i class="layui-icon">&#x1006;</i></div>

        <!-- 上传状态 -->
        <div class="upload__resource__notify upload__resource__notify--none"><span></span></div>
    </div>
    
    <!-- 文件组件对应样式 -->
    <div class="upload__item">
        <!-- 资源模块 -->
        <div class="upload__file">
            <span>test.xls</span>
        </div>

        <!-- 操作按钮 -->
        <div class="upload__resource__action_btn upload__resource__action_btn--left"><i class="layui-icon">&#xe603;</i></div>
        <div class="upload__resource__action_btn upload__resource__action_btn--right"><i class="layui-icon">&#xe602;</i></div>
        <div class="upload__resource__action_btn upload__resource__action_btn--close"><i class="layui-icon">&#x1006;</i></div>

        <!-- 上传状态 -->
        <div class="upload__resource__notify upload__resource__notify--none"><span></span></div>
    </div>
```

```js
    // js部分

    import { uploadFilesApi } from "/static/admin/js/extends/layui-upload-files.js";

    // 赋值上传组件
    let upload = layui.upload;
    uploadFilesApi.upload = upload;
    
    // 图片上传
    uploadFilesApi.image({
        elem: '#select-files-img',
        bindAction: '#upload-files',
        resetAction: "#reset-files-img",
    }, {
        field: 'group',

        resource: {
            list: [
                {
                    url: 'http://www.think-back.com/storage/resources/images/2020-09-17/30/031a8836fbe4a7fa6cc2ba5a7217832b88cabd.jpg',
                    input: 'resources/images/2020-09-17/30/031a8836fbe4a7fa6cc2ba5a7217832b88cabd.jpg',
                }
            ],
        },
    });

    // 视频上传
    uploadFilesApi.video({
        elem: '#select-files-img',
        bindAction: '#upload-files',
        resetAction: "#reset-files-img",
    }, {
        field: 'group',

        resource: {
            list: [
                {
                    url: 'http://www.think-back.com/storage/resources/images/2020-09-17/30/031a8836fbe4a7fa6cc2ba5a7217832b88cabd.mp4',
                    input: 'resources/images/2020-09-17/30/031a8836fbe4a7fa6cc2ba5a7217832b88cabd.mp4',
                }
            ],
        },
    });
    
    // 文件上传
    uploadFilesApi.file({
        elem: '#select-files-img',
        bindAction: '#upload-files',
        resetAction: "#reset-files-img",
    }, {
        field: 'group',

        resource: {
            list: [
                {
                    url: 'http://www.think-back.com/storage/resources/images/2020-09-17/30/031a8836fbe4a7fa6cc2ba5a7217832b88cabd.xls',
                    input: 'resources/images/2020-09-17/30/031a8836fbe4a7fa6cc2ba5a7217832b88cabd.xls',
                }
            ],
        },
    });
    
```

> 上传组件：（分片上传请在 `config` 中加 `sheet:true`）


#### 3、省市区联动组件

```html
    <!-- HTML结构 -->
    <div class="layui-form-item x-city" id="address">
        <label class="layui-form-label">地区</label>
        <div class="layui-input-inline">
            <select name="province_id" data-name="province" lay-filter="province_filter">
                <option value="">请选择省</option>
            </select>

            <select name="city_id" data-name="city" lay-filter="city_filter">
                <option value="">请选择市</option>
            </select>

            <select name="area_id" data-name="area">
                <option value="">请选择区</option>
            </select>
        </div>
    </div>

    <!-- 引入省市区 -->
    <script src="/static/admin/x-admin/js/xcity.js"></script>
```

```js
    // js部分

    // 初始化
    $('#address').xcity(form, {
        pValue: '选中的ID值，空则为0',
        cValue: '选中的ID值，空则为0',
        aValue: '选中的ID值，空则为0'
    }, {
        province: 'province_filter',
        city: 'city_filter'
    });
```
